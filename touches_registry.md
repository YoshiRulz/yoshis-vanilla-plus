(GPL) bookshelf
- tracking as https://github.com/Darkhax-Minecraft/Bookshelf/issues/223
- dep of:
	- (GPL) EnchantmentDescriptions
	- (GPL) FriendlyFire
	- (GPL) Tips
	- (GPL) ToolStats
- (minecraft:command_argument_type) bookshelf:font
- (minecraft:command_argument_type) bookshelf:item_output

(GPL) creativecore
- tracking as https://github.com/CreativeMD/CreativeCore/issues/203
- dep of:
	- (GPL) ItemPhysicLite
- (minecraft:menu) creativecore:container

(GPL) viafabricplus
- (minecraft:particle_type) viafabricplus:footstep

(Expat) fallingleaves
- (minecraft:particle_type) fallingleaves:falling_leaf
- (minecraft:particle_type) fallingleaves:falling_leaf_conifer
- (minecraft:particle_type) fallingleaves:falling_snow

(Expat) particlerain
- (minecraft:particle_type) particlerain:desert_dust
- (minecraft:particle_type) particlerain:rain_drop
- (minecraft:particle_type) particlerain:snow_flake
- (minecraft:sound_event) particlerain:weather.sandstorm
- (minecraft:sound_event) particlerain:weather.sandstorm.above
- (minecraft:sound_event) particlerain:weather.snow
- (minecraft:sound_event) particlerain:weather.snow.above

(Expat) visuality
- (minecraft:particle_type) visuality:big_slime_blob
- (minecraft:particle_type) visuality:bone
- (minecraft:particle_type) visuality:charge
- (minecraft:particle_type) visuality:emerald
- (minecraft:particle_type) visuality:feather
- (minecraft:particle_type) visuality:medium_slime_blob
- (minecraft:particle_type) visuality:small_slime_blob
- (minecraft:particle_type) visuality:soul
- (minecraft:particle_type) visuality:sparkle
- (minecraft:particle_type) visuality:water_circle
- (minecraft:particle_type) visuality:wither_bone

(proprietary) visiblebarriers
- (minecraft:item) visiblebarriers:air
- (minecraft:item) visiblebarriers:bubble_column
- (minecraft:item) visiblebarriers:cave_air
- (minecraft:item) visiblebarriers:end_gateway
- (minecraft:item) visiblebarriers:end_portal
- (minecraft:item) visiblebarriers:moving_piston
- (minecraft:item) visiblebarriers:void_air

(proprietary) xaerominimap
- (minecraft:mob_effect) xaerominimap:no_cave_maps
- (minecraft:mob_effect) xaerominimap:no_cave_maps_harmful
- (minecraft:mob_effect) xaerominimap:no_entity_radar
- (minecraft:mob_effect) xaerominimap:no_entity_radar_harmful
- (minecraft:mob_effect) xaerominimap:no_minimap
- (minecraft:mob_effect) xaerominimap:no_minimap_harmful
- (minecraft:mob_effect) xaerominimap:no_waypoints
- (minecraft:mob_effect) xaerominimap:no_waypoints_harmful

(proprietary) xaeroworldmap
- (minecraft:mob_effect) xaeroworldmap:no_cave_maps
- (minecraft:mob_effect) xaeroworldmap:no_cave_maps_harmful
- (minecraft:mob_effect) xaeroworldmap:no_world_map
- (minecraft:mob_effect) xaeroworldmap:no_world_map_harmful
