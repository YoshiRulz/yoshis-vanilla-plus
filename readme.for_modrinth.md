A vanilla-compatible&dagger; modpack focused on bugfixes
and on enhancing existing features while keeping Minecraft's visual style.
All included mods are Free Software (or public domain). Uses the Fabric loader.

&dagger; 'Vanilla-compatible' meaning that you can "Open to LAN" and have vanilla clients join, and vice-versa.

The theme of the pack is something like: what post-Caves and Cliffs Minecraft could be, if Mojang
devoted all their devs to performance and quality-of-life features rather than decorative blocks and useless mobs.
So all the vanilla mechanics and interfaces remain, they're just a bit more polished, and on top of that
there are a few modded Minecraft staples that Mojang will probably never add, such as the animations and extra HUD.

No shaders or texture packs are included. [Iris](https://modrinth.com/mod/iris) probably works if you want to add that.
For anyone looking for a little more fidelity but in the familiar post-1.14 (JAPPA) style, the resource pack [VanillaXBR](https://modrinth.com/resourcepack/vanillaxbr)
is a good fit. It uses an upscaling technique designed for retro game emulation on the textures of every block and item.

Insofar as this modpack comprises a 'derivative work', I as its creator declare it to be free for everyone, forever,
under the terms of the GNU Affero General Public License, either version 3, or any later version.
I won't accept payment for the small amount of maintenance work involved here, but if you're a content creator
and feel you should be giving back to the toolmakers who power your work, I've included "fund" links for many mods below. <!-- ...but not for proprietary or Expat-licensed projects, nor pointless "personal libraries". Also no PayPal. -->

I intend to do periodic updates, and only for the latest version of Minecraft (since you can use ViaFabricPlus).
There may be a beta period immediately after a Minecraft update drops as developers fix and re-release their mods.
I use GNU/Linux (specifically NixOS), so I can't guarantee no Windows- or macOS-specific bugs are left in the game,
but if you do find one and know of a fix I don't mind including those.

The [GitLab repo for the pack](https://gitlab.com/YoshiRulz/yoshis-vanilla-plus) is where you can:
- report bugs
- request changes or additions
- view the "source" of the pack manifest

Accounts there are free, but you can also create a thread via email.

## Default keybinds

- F1: Toggle HUD visibility (vanilla)
- F2: Unbound (would be screenshot in vanilla)
- F3 (alone): Toggle debug overlay (vanilla)
- *With RebindAllTheKeys*&lozf;:
	- F3+A: Unbound (reload all chunks has been moved to F3+F)
	- F3+D: Unbound (would be clear chat in vanilla)
	- F3+F: Reload all chunks (vanilla; the render distance increment that's usually here is not bound)
	- F3+M: Switch gamemode (vanilla)
	- F3+S: Unbound (would be create texture dump in vanilla)
	- F3+F4: Unbound (switch gamemode has been moved to F3+M)
- F4: Toggle freecam (Freecam&lozf;)
	- *This does conflict with F3+F4 unless RebindAllTheKeys*&lozf; *is also installed*
- F5: Toggle camera viewpoint (vanilla)
- F6: Toggle rendering invisible blocks (Visible Barriers&lozf;)
- F7: Toggle light level overlay (MiniHUD)
- F8: Unbound (would be toggle camera rotation smoothing in vanilla)
- F9: Toggle fullbright (Gamma Utils)
- F10: Fix "ghost blocks" (AntiGhost&lozf;)
- F11: Toggle fullscreen
- F12: Unbound
- 0: Sort inventory under cursor (Mouse Wheelie)
- Hyphen '-': Zoom out minimap (Xaero's Minimap&lozf;)
- Equals '=': Zoom in minimap (Xaero's Minimap&lozf;)
- P: Unbound (would be report player in vanilla)
- Backslash '\\': Toggle minimap visibility (Xaero's Minimap&lozf;)
- Z (hold): Zoom in (Zoomify)
- X: Unbound (would be load creative hotbar in vanilla)
- C: Unbound (would be save creative hotbar in vanilla)
- N: Create waypoint (Xaero's World Map&lozf;)
- M: Open map (Xaero's World Map&lozf;)
- Comma ',': Open waypoint manager (Xaero's World Map&lozf;)
- Page Up: `/tp ~ ~32 ~` (Command Macros&lozf;)
- Page Down: `/tp ~ ~-32 ~` (Command Macros&lozf;)
- NumPad1: Toggle block inspect ignore fluids (Jade&lozf;)
- NumPad0: Toggle block inspect HUD visibility (Jade&lozf;)
- *Rest as vanilla*

&lozf; If installed (not included by default)

You'll also find a lot of extra keyboard and mouse shortcuts in the inventory and container GUIs.

## Mod list

License | Mod | Functionality
--:|--:|:--
&mdash; | &mdash; | v Performance fixes
[LGPLv3](https://github.com/FoundationGames/EnhancedBlockEntities/blob/5c85fee987369595ff576238c40e8986306c3557/LICENSE.txt) | [Enhanced Block Entities](https://modrinth.com/mod/ebe) | Uses block models instead of the slower entity models for block entities when they're not animating
[LGPLv3](https://github.com/RaphiMC/ImmediatelyFast/blob/v1.2.8/LICENSE) | [ImmediatelyFast](https://modrinth.com/mod/immediatelyfast) | Optimises batching of low-level graphics calls, among other things
[LGPLv3](https://github.com/astei/krypton/blob/v0.2.6/LICENSE) | [Krypton](https://modrinth.com/mod/krypton) ([fund](https://github.com/sponsors/astei)) | Replaces large portions of the networking stack with more performant code
[LGPLv3](https://github.com/frikinjay/let-me-despawn/blob/c58a2a8048ceac628ab9d368348f396108567ce6/LICENSE) | [Let Me Despawn](https://modrinth.com/plugin/lmd) | Fixes mobs not naturally despawning because they've picked up an item
[LGPLv3](https://github.com/CaffeineMC/lithium-fabric/blob/mc1.20.4-0.12.1/LICENSE.txt) | [Lithium](https://modrinth.com/mod/lithium) ([fund](https://patreon.com/2No2Name)) | Replaces some of Minecraft's physics/gameplay routines with more efficient implementations
[LGPLv2.1](https://github.com/FxMorin/MemoryLeakFix/blob/v1.1.5/LICENSE) | [Memory Leak Fix](https://modrinth.com/mod/memoryleakfix) ([fund](https://github.com/sponsors/FxMorin)) | Fixes a couple of particularly bad RAM usage bugs which Mojang hasn't gotten around to yet
[LGPLv3](https://github.com/embeddedt/ModernFix/blob/5.12.1%2B1.20.4/LICENSE) | [ModernFix](https://modrinth.com/mod/modernfix) ([fund](https://ko-fi.com/embeddedt)) | Many fixes, including reducing RAM usage and reducing startup time
[LGPLv2.1](https://github.com/FxMorin/MoreCulling/blob/v0.22.1-1.20.1%2B/LICENSE) | [More Culling](https://modrinth.com/mod/moreculling) ([fund](https://github.com/sponsors/FxMorin)) | Skips rendering of block faces that are obscured
[LGPLv3](https://github.com/iChun/MultiplayerServerPause/blob/1b5822cce5136ec4d81e510376332366f4f5981f/COPYING.LESSER) | [Multiplayer Server Pause](https://modrinth.com/mod/multiplayer-server-pause) ([fund](https://www.patreon.com/ichun)) | Lets the server pause ticking if all connected players have the mod and are paused
[LGPLv3](https://github.com/Steveplays28/noisium/blob/v1.0.2%2Bmc1.20.x/LICENSE) | [Noisium](https://modrinth.com/mod/noisium) ([fund](https://ko-fi.com/steveplays)) | Optimises generation of new chunks
[LGPLv3](https://github.com/MCRcortex/nvidium/blob/156eff2e9ed10d6c99e2dbeb8c8c7d6fe2519f24/LICENSE.txt) | [Nvidium](https://modrinth.com/mod/nvidium) ([fund](https://www.patreon.com/mcrcortex)) | Hardware-accelerated rendering improvements (only affects Nvidia GPUs, specifically *Turing* or newer chipsets)
[LGPLv3](https://github.com/CaffeineMC/sodium-fabric/blob/mc1.20.3-0.5.6/COPYING.LESSER) | [Sodium](https://modrinth.com/mod/sodium) ([fund](https://patreon.com/jellysquid)) | Replaces the rendering engine with one more suited to modern GPUs
&mdash; | &mdash; | v Bugfixes
[LGPLv3](https://github.com/varun7654/ClientSideNoteblocks/blob/d3cbaa203e558c01c3d8584a53138a3f52c1c04a/LICENSE.md) | [Client Side Noteblocks](https://modrinth.com/mod/clientsidenoteblocks) | Plays noteblock samples when the client sees them activate, not when the sound event is received from the server
[LGPLv3](https://github.com/isXander/Debugify/blob/1.20.4%2B1.0/LICENSE) | [Debugify](https://modrinth.com/mod/debugify) ([fund](https://ko-fi.com/isxander)) | Implements obvious fixes for dozens of bugs that Mojang is too lazy to fix, and disables telemetry
[LGPLv3](https://github.com/Fallen-Breath/fast-ip-ping/blob/v1.0.1/LICENSE) | [fast-ip-ping](https://modrinth.com/mod/fast-ip-ping) | Removes a blocking networking call when connecting to LAN servers which only adds delay
[LGPLv3](https://github.com/altrisi/FastOpenLinksAndFolders/blob/1.1.1/LICENSE) | [FastOpenLinksAndFolders](https://modrinth.com/mod/fastopenlinksandfolders) | Fixes huge delays on `xdg-open`-like features (`Runtime.exec`), such as "Open Resource Pack Folder..."
[LGPLv2.1](https://github.com/Darkhax-Minecraft/Friendly-Fire/blob/8a9f8dd3b8a9c09d649d9142ba613c1d7059c39c/LICENSE) | [Friendly Fire](https://modrinth.com/mod/friendly-fire) ([fund](https://www.patreon.com/Darkhax)) | Prevents you from accidentally attacking your pets
[GPLv3](https://github.com/MehVahdJukaar/modelfix-multi/blob/ae478af732199aeae8d405ea50f601bd0bbdedb1/LICENSE.md) | [Model Gap Fix](https://modrinth.com/mod/modelfix) | Implements obvious fix for MC-73186 re: held item rendering, which Mojang is too lazy to fix
[LGPLv3](https://github.com/shedaniel/your-options-shall-be-respected/blob/6ede5aa418f23a6601a8a6de3ceeb145b266c082/LICENSE) | [Your Options Shall Be Respected](https://modrinth.com/mod/yosbr) ([fund](https://github.com/sponsors/shedaniel)) | Makes default config overrides only apply on first install (or when adding mods), not when updating
&mdash; | &mdash; | v UI/UX improvements
[LGPLv3](https://github.com/isXander/AdaptiveTooltips/blob/1.3.0/LICENSE) | [AdaptiveTooltips](https://modrinth.com/mod/adaptive-tooltips) ([fund](https://ko-fi.com/isxander)) | Improves item and button tooltip placement, especially for modded items with lots of flavourtext
[Unlicense](https://github.com/squeek502/AppleSkin/blob/v2.5.1/LICENSE) | [AppleSkin](https://modrinth.com/mod/appleskin) ([fund](https://github.com/sponsors/squeek502)) | Improves hunger-related HUD and adds hunger points to consumables' tooltips
[CC0](https://github.com/Tectato/BetterCommandBlockUI/blob/22ea05f6c106e49672367e847496c87cee798d77/LICENSE) | [Better Command Block UI](https://modrinth.com/mod/bettercommandblockui) | Replaces the command block's interface
<!-- newer versions relicensed to Expat :( also "applies to" tooltips feature crashes with Iceberg(?) --> [Unlicense](https://github.com/Bernasss12/BetterEnchantedBooks/blob/5513c3fb3924f5495358b328da82caf5fa3de333/LICENSE) | [Better Enchanted Books](https://modrinth.com/mod/better-enchanted-books) | Colours enchanted books so you can tell enchantments apart
[GPLv3](https://github.com/Lortseam/better-mount-hud/blob/3ea29431d19432f6eff1e255b3fa521c3f0218d9/LICENSE) | [Better Mount HUD](https://modrinth.com/mod/better-mount-hud) | Re-enables food and XP HUDs that are hidden when mounted
[LGPLv3](https://github.com/50ap5ud5/BoatItemView/blob/15da71f70667bae6fb0d98f84add9be61cbf749e/LICENSE) | [Boat Item View](https://modrinth.com/mod/boat-item-view) | Re-enables rendering the held item while rowing a boat
[Unlicense](https://github.com/JustAPotota/fabric-breakfree/blob/7e24b3b9cea517ea51e8f4822ba7c2c5222fe61a/LICENSE) | [Break Free](https://modrinth.com/mod/breakfree) | Makes block break progress not reset if you switch tools, reverting a change from 1.4.4 (yes that's over a decade ago and I'm still salty)
[LGPLv3+](https://github.com/illusivesoulworks/cakechomps/blob/45cb6dbe6013a3369b0a48cd5eefc9a571a5121b/gradle.properties#L14) | [Cake Chomps](https://modrinth.com/mod/cake-chomps) ([fund](https://ko-fi.com/theillusivec4)) | Adds audio and additional visual feedback to eating cakes
[CC0](https://github.com/Ezzenix/ChatAnimation/blob/6ed86286471101d46980a350bc4caad5a76d5118/LICENSE) | [Chat Animation](https://modrinth.com/mod/chatanimation) | Adds an opening animation to chat and animates incoming messages
[MPL2](https://github.com/dzwdz/chat_heads/blob/0.10.31/LICENSE) | [Chat Heads](https://modrinth.com/mod/chat-heads) | Displays players' faces in chat to match the player list
[LGPLv3](https://github.com/mrbuilder1961/ChatPatches/blob/f5d00a807092627f60bc7ce31aab1b4ec0bb7991/LICENSE) | [Chat Patches](https://modrinth.com/mod/chatpatches) ([fund](https://ko-fi.com/obro1961)) | Buffs the in-game chat scrollback buffer, among other things
[LGPLv3+](https://github.com/illusivesoulworks/cherishedworlds/blob/f01672877bc3a9833a282377850686397d8c123a/COPYING.LESSER) | [Cherished Worlds](https://modrinth.com/mod/cherished-worlds) ([fund](https://ko-fi.com/C0C1NL4O)) | Allows marking saves as favourites, pinning them to the top of the list
[LGPLv3](https://github.com/mrmelon54/draggable_lists/blob/v1.0.5%2B1.20.4/LICENSE.md) | [Draggable Lists](https://modrinth.com/mod/draggable-lists) ([fund](https://ko-fi.com/mrmelon54)) | Makes the resource pack and server lists drag-and-drop
[LGPLv2.1](https://github.com/Darkhax-Minecraft/Effect-Tooltips/blob/08bb0499d2ed92703120fe9d90c50aadf08ac73d/LICENSE) | [Effect Tooltips](https://modrinth.com/mod/effect-tooltips) ([fund](https://www.patreon.com/Darkhax)) | Adds tooltips to the potion effect indicators in the inventory
[LGPLv2.1](https://github.com/Darkhax-Minecraft/Enchantment-Descriptions/blob/11c5c74e7e0ae6b0089be4119c163189e4481a29/gradle.properties#L30) | [Enchantment Descriptions](https://modrinth.com/mod/enchantment-descriptions) ([fund](https://www.patreon.com/Darkhax)) | Includes a description of each enchantment in the tooltips of enchanted books
[LGPLv3+](https://github.com/n643064/Harvest-Fabric/blob/1.3/LICENSE) | [Harvest](https://modrinth.com/mod/harvest) | Allows "smart harvest" of crops with right-click
[LGPLv3](https://github.com/A5b84/held-item-info/blob/v1.5.0/COPYING.LESSER) | [Held Item Info](https://modrinth.com/mod/held-item-info) | Adds important details to the held item name above the hotbar
[LGPLv3](https://github.com/Parzivail-Modding-Team/HereBeNoDragons/blob/c92d8e110347bcee985e648f7ac2be5e3335b8e3/LICENSE) | [Here be no Dragons!](https://modrinth.com/mod/here-be-no-dragons) | Removes the confirmation screen when loading worlds with custom worldgen datapacks
[Apache2](https://github.com/Siphalor/mouse-wheelie/blob/v1.13.3%2Bmc1.20.4/LICENSE) | [Mouse Wheelie](https://modrinth.com/mod/mouse-wheelie) | Adds a few inventory management features
[MPL2](https://gitlab.com/Raspberry-Jam/pinglist/-/blob/1.4/LICENSE) | [PingList](https://modrinth.com/mod/pinglist) | Replaces the "reception bars" in the player list with a numeric readout
[LGPLv2.1](https://github.com/Steveplays28/realisticsleep/blob/v1.10.2%2Bmc1.20.3-1.20.4/LICENSE) | [Realistic Sleep](https://modrinth.com/mod/realisticsleep) ([fund](https://ko-fi.com/steveplays)) | Makes sleeping fast-forward time instead of skipping time
[LGPLv3+](https://github.com/xiaocihua/stack-to-nearby-chests/blob/mc1.20.4-0.5.4/LICENSE) | [Stack to Nearby Chests](https://modrinth.com/mod/stack-to-nearby-chests) | Adds a few simple storage management features
[LGPLv3](https://github.com/A5b84/status-effect-bars/blob/v1.0.4/COPYING.LESSER) | [Status Effect Bars](https://modrinth.com/mod/status-effect-bars) | Adds a subtle duration meter to status effect indicators
[LGPLv2.1](https://github.com/Darkhax-Minecraft/Tool-Stats/blob/68c2bef0a95fa5d8c6bbe13cb45e0460d03dd0be/LICENSE) | [Tool Stats](https://modrinth.com/mod/tool-stats) ([fund](https://www.patreon.com/Darkhax)) | Includes basic details in the tooltips of tools and armour which are missing from vanilla
&mdash; | &mdash; | v Other changes and additions
[GPLv3](https://github.com/A5b84/armor-chroma-fabric/blob/v1.2.6/LICENSE) | [Armor Chroma](https://modrinth.com/mod/armor-chroma-for-fabric) | Improves the armour points HUD
[LGPLv3+](https://github.com/Johni0702/bobby/blob/v5.0.3/LICENSE.LESSER.md) | [Bobby](https://modrinth.com/mod/bobby) | Caches chunks that fall out of render distance, and allows increasing the max. render distance
[GPLv3](https://github.com/kerudion/chunksfadein/blob/1e5f7ff6aac4796dd8b8a36dacf7c9e5c666846b/LICENSE) | [Chunks fade in](https://modrinth.com/mod/chunks-fade-in) | Adds an animation to the initial rendering of chunks
[LGPLv3](https://github.com/Sjouwer/gamma-utils/blob/5fdd7d4c8839d4c08db564e6ea99f576e5537672/LICENSE) | [Gamma Utils](https://modrinth.com/mod/gamma-utils) | Adds a toggleable 'fullbright' mode, which allows you to see in the dark
[GPLv3](https://github.com/DaFuqs/HeadInTheClouds/blob/3ba048b290d59ac2c1eae9559ef7cd61191333cd/LICENSE) | [Head in the Clouds](https://modrinth.com/mod/head-in-the-clouds) ([fund](https://ko-fi.com/dafuqs)) | Makes it not rain above where the clouds are rendered
[LGPLv3](https://github.com/CreativeMD/ItemPhysicLite/blob/81442798c8b072335a25eeb67049478b8095e531/LICENSE) | [ItemPhysic Lite](https://modrinth.com/mod/itemphysic-lite) ([fund](https://www.patreon.com/creativemd)) | Changes dropped items to sit naturally on the ground rather than float and spin
[LGPLv3](https://github.com/iChun/LetSleepingDogsLie/blob/e6edcdc43db09f8223ddfcf5024600f7e90f660c/COPYING.LESSER) | [Let Sleeping Dogs Lie](https://modrinth.com/mod/let-sleeping-dogs-lie) ([fund](https://www.patreon.com/ichun)) | Gives sitting wolves a lying down animation, used when no-one's around
[LGPLv3](https://github.com/maruohon/minihud/blob/9fc979911b1c45834e11625f231e93058d134aa3/LICENSE.txt) | [MiniHUD](https://www.curseforge.com/minecraft/mc-mods/minihud) (CF) | Adds configurable readouts to the corner of the screen, like a more useful debug overlay
[GPLv3+](https://github.com/juancarloscp52/panorama-screens/blob/4ae843f3214beffb830fd6114e8bd50ba83eee3a/LICENSE) | [Panorama Screens](https://modrinth.com/mod/panorama-screens) | Uses the panorama from the main menu instead of a dirt background for most menus
[GPLv3](https://github.com/spoorn/SeeThroughLava/blob/4d1bb515d113ff793ece468a16d29521d7ea372c/LICENSE) | [See Through Water/Lava](https://modrinth.com/mod/see-through-waterlava) | Increases visibility in water and lava
[CC0](https://github.com/Draradech/SimpleFogControl/blob/v1.4.0/LICENSE) | [Simple Fog Control](https://modrinth.com/mod/simplefog) | Increases visibility in the nether and fixes the discolouration of distant chunks
[LGPLv2.1](https://github.com/Darkhax-Minecraft/Tips/blob/f2853ad5610c9abdf3f374f146019b9d83662cc6/LICENSE) | [Tips](https://modrinth.com/mod/tips) ([fund](https://www.patreon.com/Darkhax)) | Adds protips to the pause and loading screens
[CC0](https://github.com/Tectato/Vectorientation/blob/fe71f9bac8c840307424cad61d9b6a25652d05f2/LICENSE) | [Vectorientation](https://modrinth.com/mod/vectorientation) | Gives falling blocks a fancy animation
[GPLv3](https://github.com/ViaVersion/ViaFabricPlus/blob/v3.0.6/LICENSE) | [ViaFabricPlus](https://modrinth.com/mod/viafabricplus) ([fund](https://viaversion.com/donate)) | Allows connecting to servers running older versions of Minecraft
[GPLv3](https://github.com/Wudji/XPlus-AutoFish/blob/e7e9e8b6d2a573a096439e63177280794c4af435/LICENSE) | [XPlus Autofish](https://modrinth.com/mod/x+-autofish) | Automates fishing (arguably cheating)
[LGPLv3](https://github.com/isXander/Zoomify/blob/2.13.0/LICENSE) | [Zoomify](https://modrinth.com/mod/zoomify) ([fund](https://ko-fi.com/isxander)) | Makes spyglass zoom configurable and optionally usable without a spyglass
&mdash; | &mdash; | v Libraries
[LGPLv3](https://github.com/architectury/architectury-api/blob/93c804fe070f34998df3c73086620c0cc42b4049/LICENSE.md) | [Architectury API](https://modrinth.com/mod/architectury-api) ([fund](https://patreon.com/shedaniel)) |
[LGPLv2.1](https://github.com/Darkhax-Minecraft/Bookshelf/blob/85c5de00cff2f28cd3cfc5c0948f9bc79209e8f8/License.md) | [Bookshelf](https://modrinth.com/mod/bookshelf-lib) ([fund](https://www.patreon.com/Darkhax)) |
[LGPLv3](https://github.com/shedaniel/cloth-config/blob/bc60b2f30c84f7f4df580092bd76017d48bd7abb/LICENSE.md) | [Cloth Config API](https://modrinth.com/mod/cloth-config) ([fund](https://github.com/sponsors/shedaniel)) |
[Apache2](https://github.com/Lortseam/completeconfig/blob/2.5.3/LICENSE) | [CompleteConfig](https://modrinth.com/mod/completeconfig) |
[LGPLv3](https://github.com/CreativeMD/CreativeCore/blob/7489c241c8b510f2f482d5b5c804879e30852b3a/LICENSE) | [CreativeCore](https://modrinth.com/mod/creativecore) |
[Apache2](https://github.com/FabricMC/fabric/blob/0.95.3%2B1.20.4/LICENSE) | [Fabric API](https://modrinth.com/mod/fabric-api) |
[Apache2](https://github.com/FabricMC/fabric-language-kotlin/blob/1.10.17%2Bkotlin.1.9.22/LICENSE) | [Fabric Language Kotlin](https://modrinth.com/mod/fabric-language-kotlin) |
[MPL2](https://github.com/Fuzss/forgeconfigapiport/blob/ca7e68fc19734be125d0abec98b625f86e1d3624/LICENSE.md) | [Forge Config API Port](https://modrinth.com/mod/forge-config-api-port) |
[Apache2](https://github.com/comp500/Indium/blob/1.0.28%2Bmc1.20.4/LICENSE) | [Indium](https://modrinth.com/mod/indium) |
[LGPLv3](https://github.com/maruohon/malilib/blob/4991ed86ac8a92a00fa16117a8ae39dc74ce6bb9/LICENSE.txt) | [MaLiLib](https://modrinth.com/mod/malilib) |
[LGPLv3](https://github.com/isXander/YetAnotherConfigLib/blob/3.3.2%2B1.20.4/LICENSE) | [YetAnotherConfigLib](https://modrinth.com/mod/yacl) ([fund](https://ko-fi.com/isxander)) |

<!-- TODO ship protip resource packs as mods -->

Additional suggested mods (NOT included, but some default configs are):

<!-- waiting for finished or unarchived:
- (CC BY 4.0) [Better Falling Block Particles](https://modrinth.com/mod/better-falling-block-particles): [touches registry on server](https://github.com/Enchanted-Games/falling-block-particles/issues/1)
- [Concurrent Chunk Management Engine](https://modrinth.com/mod/c2me-fabric)
- ([LGPLv3](https://github.com/alphaqu/DashLoader/blob/e14ad8f166baadfc1d3b0c47730b70d68c243040/LICENCE)) [DashLoader](https://modrinth.com/mod/dashloader): Caches asset loading to make the game launch faster, since that's usually unchanged between launches; doesn't work, even with deps override
- [Distant Horizons](https://modrinth.com/mod/distanthorizons): Uses modern LOD techniques to make increased render distances feasible (Bobby alternative)
- (Expat) [EMI Loot](https://modrinth.com/mod/emi-loot): doesn't work, even with deps override
- (Expat) [EMIffect](https://modrinth.com/mod/emiffect): doesn't work, even with deps override
- (Expat) [Explosive Enhancement](https://modrinth.com/mod/explosive-enhancement): [touches registry on server](https://github.com/Superkat32/Explosive-Enhancement/issues/22)
- [FastAnim](https://modrinth.com/mod/fastanim): Optimises entity animation (pre-rendering) routines; available already, but the (Expat-licensed) source repo is missing from [the modder's GitHub profile](https://github.com/AViewFromTheTop)
- ([GPLv3](https://github.com/BumbleSoftware/Fastload/blob/9a4046bba6b142ce65d096dd3e816a2a1db995b0/LICENSE)) [Fastload](https://modrinth.com/mod/fastload): Disables a routine which forces you wait for all the chunks around you to be loaded before rendering or giving you control; doesn't work, even with deps override
- (MPL2) [Mindful Darkness](https://modrinth.com/mod/mindful-darkness): auto dark theme; doesn't work, even with deps override
- [Particle Moths](https://modrinth.com/mod/particle-moths): adds moths; doesn't work, even with deps override
- [Simple Voice Chat](https://modrinth.com/plugin/simple-voice-chat)
- [Very Many Players](https://modrinth.com/mod/vmp-fabric)
- [VulkanMod](https://modrinth.com/mod/vulkanmod): Switches GPU interface to Vulkan; incompatible with [several included mods](https://github.com/xCollateral/VulkanMod/discussions/226)
- (MPL2) [WagYourMinimap](https://modrinth.com/mod/wagyourminimap): doesn't work, even with deps override
-->

License | Mod | Cat. | Functionality
--:|--:|:--|:--
[Expat + NC clause](https://github.com/tr7zw/3d-Skin-Layers/blob/1.6.2/LICENSE) | [3D Skin Layers](https://modrinth.com/mod/3dskinlayers) | fixes | Fixes the "hat" layer of players' skins (by adding sides) so they don't appear to be floating
[MIT (Expat)](https://github.com/SpaceWalkerRS/alternate-current/blob/v1.7/LICENSE) | [Alternate Current](https://modrinth.com/mod/alternate-current) | perf | Optimises power/signal propagation through redstone dust lines
[MIT (Expat)](https://github.com/jaredlll08/Ambient-Environment/blob/b3512d93d47f24daa26bd60ca9ce74b07aee191d/fabric/src/main/resources/fabric.mod.json#L14) | [Ambient Environment](https://modrinth.com/mod/ambient-environment) | enh. | Introduces noise to the foliage and water biome colour gradients
[MIT (Expat)](https://github.com/gbl/AntiGhost/blob/3767ea6fa499568df4d3da1f1f428b5f6678ec48/LICENSE) | [AntiGhost](https://modrinth.com/mod/antighost) | fixes | Adds a keybind to re-request nearby block data from the server for when the client becomes desynced ("ghost blocks")
[MIT (Expat)](https://github.com/thebrightspark/AsyncLocator/blob/1.20-1.3.0/LICENSE) | [Async Locator](https://modrinth.com/mod/async-locator) | perf | Optimises the structure pathfinding routine used for dolphins and `/locate`
[MIT (Expat)](https://github.com/axieum/authme/blob/v8.0.0%2B1.20.4/LICENCE.txt) | [Auth Me](https://modrinth.com/mod/auth-me) | enh. | Allows re-authenticating from within the game
[MIT (Expat)](https://github.com/ItsThosea/BadOptimizations/blob/cf6c12acc56873a462ebd8682c4c368498eb3f60/LICENSE) | [BadOptimizations](https://modrinth.com/mod/badoptimizations) | perf | Skips some idempotent calls in the pre-rendering chain, among other things
<!-- lib [source-available proprietary](https://github.com/TwelveIterationMods/Balm/blob/v9.0.5/LICENSE) | [Balm](https://modrinth.com/mod/balm) | lib | -->
<!-- (not going to suggest what's essentially a resource pack here, but I will include it in the impure pack) MIT (Expat) | [Better End Sky](https://modrinth.com/mod/better-end-sky) | enh. | Replaces the End's skybox with the one from the Better End mod -->
<!-- (not going to suggest what's essentially a resource pack here, but I will include it in the impure pack) LGPLv3 | [Better Than Llamas](https://modrinth.com/mod/better-than-llamas) | enh. | Adds fezzes and monocles to llama models -->
[MIT (Expat)](https://github.com/melontini/blame-log/blob/0.4.0-1.18/LICENSE) | [BlameLog](https://modrinth.com/mod/blame-log) | fixes | Improves other mods' logging to help identify which mod is misbehaving
[CC BY-NC-SA 4.0](https://github.com/shmove/cat_jam/blob/dbafa947a2d71653377c4d0637f38d09f91663a0/LICENSE.md) | [cat_jam](https://modrinth.com/mod/cat_jam) | enh. | Adds dancing animations to cats like parrots have, so you can catJAM
[MIT (Expat)](https://github.com/uku3lig/chathighlighter/blob/6597010a885cf538935858c2fd9fadcaa8b71e42/LICENSE) | [ChatHighlighter](https://modrinth.com/mod/chathighlighter) | UI/UX | Allows highlighting mentions of your username (nickping), or any other keyword, in chat
<!-- ([not working with ItemPhysic Lite at the moment](https://github.com/StrikerRockers-Mods/ClearDespawn/issues/17)) [MIT (Expat)](https://github.com/StrikerRockers-Mods/ClearDespawn/blob/1.20.2-1.1.15/LICENSE) | [ClearDespawn](https://modrinth.com/mod/cleardespawn) | UI/UX | Displays dropped items' lifetimes in-world (as a blinking effect on items close to despawning) -->
<!-- (can't endorse this due to the license; though older versions of the mod were Expat-licensed) [source-available proprietary](https://github.com/TwelveIterationMods/ClientTweaks/blob/v13.0.2/LICENSE) | [Client Tweaks](https://modrinth.com/mod/client-tweaks) | UI/UX | Various changes to dual-wielding items, and brings back the master and music volume sliders -->
[MIT (Expat)](https://github.com/jaredlll08/Clumps/blob/6f56ba314adf695e0ef34f1a4d4084b9c43c1ac6/LICENSE.md) | [Clumps](https://modrinth.com/mod/clumps) | perf | Combines XP orb entities
[MIT (Expat)](https://github.com/kyrptonaught/CMDKeybinds/blob/3d4b0ec88426ad27ee479041f647ea6f4842f0aa/LICENSE) | [Command Macros](https://modrinth.com/mod/command-macros) | enh. | Allows sending arbitrary chat messages and commands with keyboard shortcuts
<!-- (can't endorse this due to the license) [source-available](https://github.com/Snownee/Companion/tree/ea2cf0d517c2c2b04db5e0dd2b4ace441c0e5d2c) proprietary | [Companion](https://modrinth.com/mod/companion) | enh. | Improves pet following and combat mechanics -->
[MIT (Expat)](https://github.com/jaredlll08/Controlling/blob/76887d7bf46ece88635ea0d1a283eda0bf1fc112/LICENSE) | [Controlling](https://modrinth.com/mod/controlling) | UI/UX | Adds a search box to the keybinds menu
[CC BY-NC-ND 4.0](https://github.com/AHilyard/CooperativeAdvancements/blob/768d5678a10a9afbbef0c3670fd96a2acbd2177e/LICENSE) | [Cooperative Advancements](https://modrinth.com/mod/cooperative-advancements) | enh. | Synchronises advancement progress between players on the same team
[MIT (Expat)](https://gitlab.com/CDAGaming/CraftPresence/-/blob/release/v2.3.0/LICENSE) + 3p | [CraftPresence](https://modrinth.com/mod/craftpresence) | enh. | Adds "rich presence" for Discord
[MIT (Expat)](https://github.com/juliand665/Dynamic-FPS/blob/3.3.3/LICENSE) | [Dynamic FPS](https://modrinth.com/mod/dynamic-fps) | perf | Adds a separate framerate limit for when Minecraft is unfocused
[MIT (Expat)](https://git.skye.vg/me/e4mc_minecraft/src/commit/ccb7025b74811f05b1de49cad3dc0960a0d3edba/LICENSE) | [e4mc](https://modrinth.com/mod/e4mc) | enh. | Enhances "Open to LAN" with a reverse proxy ([e4mc.link](https://e4mc.link)) so your server can be accessed from anywhere, even if you're behind a firewall/NAT/whatever
[MIT (Expat)](https://github.com/emilyploszaj/emi/blob/1.1.0%2B1.20.4/LICENSE) | [EMI](https://modrinth.com/mod/emi) | enh. | Adds a superpowered version of the recipe book, with an index of every item in the game and their recipes
[MIT (Expat)](https://github.com/fzzyhmstrs/EMI_enchanting/blob/a7bfd1a64985cd0be04339260fb5196b776297c4/src/main/resources/fabric.mod.json#L16) | [EMI Enchanting](https://modrinth.com/mod/emi-enchanting) | enh. | Enchanting plugin for EMI
[MIT (Expat)](https://github.com/Prismwork/EMITrades/blob/21fbf13c1ceeeae0da82ba9f4a6cbd6cde40aa68/LICENSE) | [EMI Trades](https://modrinth.com/mod/emitrades) | enh. | Villager trade plugin for EMI
[MIT (Expat)](https://github.com/Minenash/Enhanced-Attack-Indicator/blob/v1.0.4%2B1.20.3/LICENSE) | [Enhanced Attack Indicator](https://modrinth.com/mod/enhanced-attack-indicator) | UI/UX | Adds more functionality to the attack indicator
[Expat + NC clause](https://github.com/tr7zw/EntityCulling/blob/1.6.3.1-1.20.4/LICENSE-EntityCulling) | [EntityCulling](https://modrinth.com/mod/entityculling) | perf | Skips rendering entities that would be obscured by a wall
[CC BY-NC-ND 4.0](https://github.com/AHilyard/EquipmentCompare/blob/e95dbfa89b0cfbc155ff65929c2dce8788aa992d/LICENSE) | [Equipment Compare](https://modrinth.com/mod/equipment-compare) | UI/UX | Shows a tooltip for the currently-equipped armour when looking at the tooltip for other armour
[Expat + NC clause](https://github.com/tr7zw/Exordium/blob/1.2.1-1.20.4/LICENSE) | [Exordium](https://modrinth.com/mod/exordium) | perf | Renders GUI at a lower framerate than the game world
[MIT (Expat)](https://github.com/alphaqu/extended-clouds/blob/7cc4c3ebd5036064bb9cf38cc9da4f2f1bd25657/LICENSE) | ~~[Extended Clouds](https://modrinth.com/mod/extended-clouds)~~ ([breaks with Sodium](https://github.com/alphaqu/extended-clouds/issues/11)) | enh. | Renders more of the cloud layer so it doesn't look like distant chunks have no clouds
[MIT (Expat)](https://github.com/RandomMcSomethin/fallingleaves/blob/a3d5ee0ff6dcf912cfee33478b607b9857a1f982/LICENSE) | [Falling Leaves](https://modrinth.com/mod/fallingleaves) | enh. | Adds leaf particles which spawn from leaves
[MIT (Expat)](https://github.com/KingContaria/FastQuit/blob/126727c747d661d38554059d3c3d7d8670f8fbed/LICENSE) | [FastQuit](https://modrinth.com/mod/fastquit) | UI/UX | Moves the work of closing a local world to the background
[MIT (Expat)](https://github.com/malte0811/FerriteCore/blob/build-6.0.3/LICENSE) | [FerriteCore](https://modrinth.com/mod/ferrite-core) | perf | Reduces RAM consumption by improving a few low-hanging fruit
[MIT (Expat)](https://github.com/MinecraftFreecam/Freecam/blob/1.2.3/LICENSE) | [Freecam](https://github.com/MinecraftFreecam/Freecam) ([fair play](https://modrinth.com/mod/freecam)) | enh. | Allows moving the camera around to see far-away things, like spectator mode, but without moving the player as well
[MIT (Expat)](https://github.com/Team-Resourceful/Highlight/blob/v2.2.0/LICENSE.md) | [Highlight](https://modrinth.com/mod/highlight) | fixes | Fixes block selection reticles for lecterns, signs, etc.
<!-- lib [CC BY-NC-ND 4.0](https://github.com/AHilyard/Iceberg/blob/f8f91c8a242fe9ad534575fe715c57217a0324be/LICENSE) | [Iceberg](https://modrinth.com/mod/iceberg) | lib | -->
[CC BY-NC-ND 4.0](https://github.com/AHilyard/Highlighter/blob/d20868c623e6d3178df9c85aadb5593aee529a5a/LICENSE) | [Item Highlighter](https://modrinth.com/mod/item-highlighter) | UI/UX | Highlights items which were recently picked up in the inventory
[CC BY-NC-SA 4.0](https://github.com/Snownee/Jade/blob/fabric-13.2.1/LICENSE.md) | [Jade](https://modrinth.com/mod/jade) ([fund](https://ko-fi.com/snownee)) | enh. | Adds a block inspection widget to the HUD
<!-- (can't endorse this due to the license; though older versions of the mod were Expat-licensed) [source-available proprietary](https://github.com/TwelveIterationMods/KleeSlabs/blob/v16.0.1/LICENSE) | [KleeSlabs](https://modrinth.com/mod/kleeslabs) | enh. | Only breaks one slab of a double-slab unless sneaking -->
[MIT (Expat)](https://github.com/LambdAurora/LambDynamicLights/blob/v2.3.4%2B1.20.4/LICENSE) | [LambDynamicLights](https://modrinth.com/mod/lambdynamiclights) | enh. | Makes glowing entities, entities holding a glowing block, and dropped glowing blocks illuminate their surroundings
[MIT (Expat)](https://github.com/Jerozgen/LanguageReload/blob/1.5.10%2B1.20.3/LICENSE) | [Language Reload](https://modrinth.com/mod/language-reload) | UI/UX | Replaces the single language option with a preference list
<!-- LanguageReload alternative: [lazy-language-loader](https://modrinth.com/mod/lazy-language-loader) -->
<!-- (can't endorse this due to the license) [bespoke with contradictory terms](https://github.com/Tschipcraft/make_bubbles_pop/blob/0.2.0/LICENSE) | [Make Bubbles Pop](https://modrinth.com/mod/make_bubbles_pop) | enh. | Improves the air bubble particle mechanic -->
[MIT (Expat)](https://github.com/Dyvinia/MCPings/blob/02adf3bb1f4b3c4a87700f55ab96a0dd056d3575/LICENSE) | [MCPings](https://modrinth.com/mod/mcpings) | enh. | Adds a virtual laser pointer function for indicating a location or entity to other players
<!-- lib [MIT (Expat)](https://github.com/TeamMidnightDust/MidnightLib/blob/2ff92526ba4be513a0a535e7ffb4171a9837568e/LICENSE) | [MidnightLib](https://modrinth.com/mod/midnightlib) | lib | -->
[MIT (Expat)](https://github.com/JustAlittleWolf/ModDetectionPreventer/blob/1.1.0/LICENSE) | [Mod Detection Preventer](https://modrinth.com/mod/moddetectionpreventer) | fixes | Patches a security hole which would allow the server you're on to detect whether you have a certain mod installed
[MIT (Expat)](https://github.com/TerraformersMC/ModMenu/blob/v9.0.0/LICENSE) | [Mod Menu](https://modrinth.com/mod/modmenu) | UI/UX | Adds a mod list GUI
[MIT (Expat)](https://github.com/MrLoLf/InfinityFix/blob/dcd36d2e682612165abb44239c73ad3740915869/LICENSE) | [New Infinity Fix](https://modrinth.com/mod/new-infinity-fix) | UI/UX | Removes the need to carry around a useless arrow for an Infinity bow
[WTFPL](https://github.com/Aizistral-Studios/No-Chat-Reports/blob/14150e4a67caf14cd357e631b19799cdde04cc39/LICENSE) | [No Chat Reports](https://modrinth.com/mod/no-chat-reports) ([fund](https://www.buymeacoffee.com/aizistral)) | security | Disables most of Mojang's player reporting system
[MIT (Expat)](https://github.com/semper-idem/nonvflickering/blob/15c95ce6574029a7f15576d2f2a0599133299557/LICENSE) | [No Night Vision Flickering](https://modrinth.com/mod/no-night-vision-flickering) | fixes | Replaces the Night Vision enchantment's flickering with a fade out
[MIT (Expat)](https://github.com/SpaceWalkerRS/no-resource-pack-warnings/blob/v1.3.0/LICENSE) | [No Resource Pack Warnings](https://modrinth.com/mod/no-resource-pack-warnings) | UI/UX | Removes the confirmation screen when enabling resource packs made for older versions
[LGPLv3](https://github.com/Grayray75/NoRecipeBook/blob/v3.1/LICENSE.txt) | [NoRecipeBook](https://modrinth.com/mod/norecipebook-fabric) | UI/UX | Removes the vanilla recipe book (since EMI doesn't)
<!-- TODO review all these options --> [Expat + NC clause](https://github.com/tr7zw/NotEnoughAnimations/blob/1.7.0/LICENSE) | [Not Enough Animations](https://modrinth.com/mod/not-enough-animations) | enh. | Changes some player animations
[MIT (Expat)](https://github.com/natanfudge/Not-Enough-Crashes/blob/b74d457eadf9f66ac246000376e6fbf4d2baa1c1/LICENSE) | [Not Enough Crashes](https://modrinth.com/mod/notenoughcrashes) | fixes | Makes some fatal crashes non-fatal (only sending you back to the main menu)
<!-- lib [MIT (Expat)](https://github.com/wisp-forest/owo-lib/blob/0.12.5%2B1.20.3/LICENSE) | [owo](https://modrinth.com/mod/owo-lib) | lib | -->
[MIT (Expat)](https://github.com/DaFuqs/PaginatedAdvancements/blob/6e8656733ad9cd5156573f4f9738cb0433f886c2/LICENSE) | [Paginated Advancements](https://modrinth.com/mod/paginatedadvancements) | UI/UX | Improves usability of advancement menu, especially when using datapacks/mods
<!-- PaginatedAdvancements alternative: [AdvancementInfo](https://modrinth.com/mod/advancementinfo) -->
[MIT (Expat)](https://github.com/PigCart/particle-rain/blob/v2.0.8/LICENSE) | [Particle Rain](https://modrinth.com/mod/particle-rain) | enh. | Replaces the ugly animated sprites for rain and snow with particles
[MIT (Expat)](https://github.com/PotatoPresident/PetOwner/blob/ed57b9de2ae863e26689d7a0ea039a51c0c3d65b/LICENSE) | [Pet Owner](https://modrinth.com/mod/petowner) | UI/UX | Shows who owns a pet when you look at one
[MIT (Expat)](https://github.com/Minenash/RebindAllTheKeys/blob/v1.4.0%2B1.20.2/LICENSE) | [RebindAllTheKeys](https://modrinth.com/mod/rebind-all-the-keys) | UI/UX | Allows all of the base game's hidden keybinds to be changed
[MIT (Expat)](https://github.com/FlashyReese/reeses-sodium-options/blob/mc1.20.4-1.7.1/LICENSE.txt) | [Reese's Sodium Options](https://modrinth.com/mod/reeses-sodium-options) | UI/UX | Adds GUI for configuring Sodium
<!-- TODO older version was AGPL; fork? --> [OSLv3](https://github.com/dima-dencep/rrls/blob/1.20.4-4.0.1/LICENSE) | [Remove Reloading Screen](https://modrinth.com/mod/rrls) | UI/UX | Allows interacting with the UI while resource packs are reloading
<!-- lib [MIT (Expat)](https://github.com/Team-Resourceful/ResourcefulLib/blob/v2.4.6/LICENSE) | [Resourceful Lib](https://modrinth.com/mod/resourceful-lib) | lib | -->
[MIT (Expat)](https://github.com/JustAlittleWolf/RidingMouseFix/blob/1.0.0/LICENSE) | [Riding Mouse Fix](https://modrinth.com/mod/ridingmousefix) | fixes | Fixes camera movement when mounted
<!-- lib [MIT (Expat)](https://github.com/jaredlll08/searchables/blob/52dcd1bf6ec6c7185a4dcb5d579e06210b915d4f/LICENSE) | [Searchables](https://modrinth.com/mod/searchables) | lib | -->
[MIT (Expat)](https://github.com/JustAlittleWolf/ServerPingerFixer/blob/1.0.4/LICENSE) | [Server Pinger Fixer](https://modrinth.com/mod/serverpingerfixer) | fixes | Makes loading/refreshing MotDs for the server list faster
[MIT (Expat)](https://github.com/shizotoaster/SmoothMenuRefabricated/blob/1.1.0/LICENSE) | [SmoothMenu Refabricated](https://modrinth.com/mod/smoothmenu-refabricated) | fixes | Removes the framerate restriction on the main menu
[MIT (Expat)](https://github.com/Snownee/SnowRealMagic/blob/fabric-10.3.1/LICENSE) | ~~[Snow! Real Magic!](https://modrinth.com/mod/snow-real-magic)~~ (needs updating) | enh. | Makes snowfall more well-integrated with other mechanics
<!--
Snow! Real Magic! deps:
- [MIT (Expat)](https://github.com/Snownee/Kiwi/blob/fabric-13.5.0/LICENSE) [Kiwi](https://modrinth.com/mod/kiwi)
alternatives:
- Expat https://modrinth.com/mod/stitchedsnow (snow carpet stacking)
- Expat https://modrinth.com/mod/snow-under-trees-remastered (snow under trees)
- Expat https://modrinth.com/mod/simple-snowy-fix (snow under trees)
-->
[MIT (Expat)](https://github.com/capitalistspz/SnowballKB/blob/1.2-1.20/LICENSE) | [Snowball and Egg Knockback](https://modrinth.com/mod/snowball-and-egg-knockback) | enh. | Allows using snowballs (and eggs) as a 0-damage attack against players
[MIT (Expat)](https://gitlab.com/Setadokalo/stairdoors-fabric/-/blob/28281c2940d6e6aa06bdd91203c28cff4b80b924/LICENSE) | [Stairdoors](https://modrinth.com/mod/stairdoors) | enh. | Allows placing doors on the half-block part of stairs
[MIT (Expat)](https://github.com/fxys/Super-Secret-Settings/blob/7dc34fb75d085967b4dd8cad5b18448929d4afbd/LICENSE) | [Super Secret Settings](https://modrinth.com/mod/super-secret-settings) | enh. | Restores the experimental shaders from vanilla 1.7.2
[MIT (Expat)](https://github.com/Globox1997/TalkBubbles/blob/2e01cec123e2035af678d69ed9284942cbdce022/LICENSE) | [TalkBubbles](https://modrinth.com/mod/talkbubbles) | enh. | Shows speech bubbles over players' heads when they chat
[MIT (Expat)](https://github.com/Trivaxy/Tiny-Item-Animations/blob/a137395e971974aae976672e7c07ed2d9c375e54/LICENSE) | [Tiny Item Animations](https://modrinth.com/mod/tiny-item-animations) | UI/UX | Adds a subtle animation to picking up and placing items with the cursor
[MIT (Expat)](https://github.com/Khajiitos/TradeUses/blob/050988b88161c485d4d28669f0baf7789c385074/LICENSE) | [Trade Uses](https://modrinth.com/mod/trade-uses) | UI/UX | Shows how many times each of a villager's trade offers can be used before they go "out of stock"
[MIT (Expat)](https://github.com/uku3lig/armor-hud/blob/d6857af2ef17c236d1be38f058d8bca57f4183d0/LICENSE) | [uku's Armor HUD](https://modrinth.com/mod/ukus-armor-hud) | UI/UX | Shows the armour slots in the HUD like the quickslots and offhand
<!-- lib [MPL2](https://github.com/uku3lig/ukulib/blob/1.1.0%2B1.20.3/LICENSE) | [ukulib](https://modrinth.com/mod/ukulib) | lib | -->
<!-- (can't endorse this due to the license, though I'm hoping I can convince the author to adopt AGPL) [source-available proprietary](https://github.com/AmyMialeeMods/visiblebarriers/blob/5432ec578d36d494580f0b830b8d3a2b6e1fb2ca/LICENSE) | [Visible Barriers](https://modrinth.com/mod/visiblebarriers) | enh. | Adds a toggle for making invisible blocks render similar to in creative mode, among other things -->
[MIT (Expat)](https://github.com/PinkGoosik/visuality/blob/0.7.3%2B1.20.4/LICENSE) | [Visuality](https://modrinth.com/mod/visuality) | enh. | Adds unique particles to a bunch of blocks and mobs
[LGPLv3](https://github.com/NotRyken/XaeroZoomout/blob/d757c53fb86a69d85b3503466e353356255cb11c/COPYING.LESSER) | [Xaero Zoomout](https://modrinth.com/mod/xaero-zoomout) | enh. | Allows you to zoom out farther on Xaero's World Map
<!-- (hard to endorse this, but it's integral to gameplay and there's no Free alternative) --> proprietary | [Xaero's Minimap](https://modrinth.com/mod/xaeros-minimap) ([fair play](https://modrinth.com/mod/xaeros-minimap-fair)) | enh. | Adds a configurable minimap to the HUD (which shares data with Xaero's World Map)
<!-- (ditto) --> proprietary | [Xaero's World Map](https://modrinth.com/mod/xaeros-world-map) | enh. | Adds a global map with 1-block resolution and waypoints, among other things
