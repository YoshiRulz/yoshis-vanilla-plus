let
	base = import ./options-purer.nix;
in base // {
	showSubtitles = true;
	mouseSensitivity = 0.3;
	resourcePacks = base.resourcePacks ++ [
		"file/UmActually"
		"file/Blåhaj Totem"
		"file/Laylee"
		"file/snow"
	];
	chatScale = 0.7;
	"key_key.forward" = "key.keyboard.e";
	"key_key.left" = "key.keyboard.s";
	"key_key.back" = "key.keyboard.d";
	"key_key.right" = "key.keyboard.f";
	"key_key.sprint" = "key.keyboard.a";
	"key_key.inventory" = "key.keyboard.r";
	"key_key.chat" = "key.keyboard.enter";
	"key_key.togglePerspective" = "key.keyboard.v";
	"key_key.swapOffhand" = "key.keyboard.w";
	modKeybinds = base.modKeybinds // {
		mcPings = [ "key_mcpings.key.mark-location:key.keyboard.t" ];
		rebindAllTheKeys = base.modKeybinds.rebindAllTheKeys ++ [
			"key_rebind_all_the_keys.keybind.gamemode_switcher:key.keyboard.m"
		];
	};
	modelPart_jacket = false;
	modelPart_left_sleeve = false;
	modelPart_right_sleeve = false;
}
