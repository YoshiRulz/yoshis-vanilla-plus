{
	version = 3700;
	operatorItemsTab = true;
	enableVsync = false;
	directionalAudio = true;
	bobView = false;
	darkMojangStudiosBackground = true;
	hideSplashTexts = true;
	fov = 1.0;
	screenEffectScale = 0.0;
	fovEffectScale = 0.0;
	darknessEffectScale = 0.0;
	damageTiltStrength = 0.2;
	narratorHotkey = false;
	renderDistance = 64;
	simulationDistance = 8;
	entityDistanceScaling = 5.0;
	guiScale = 2;
	maxFps = 260;
	graphicsMode = 2;
	biomeBlendRadius = 7;
	resourcePacks = [
		"vanilla"
		"fabric"
		"highlight:highlight_extended_resourcepacks/highlight_extended"
		"file/Yoshi\\u0027s Protips (Base Set)"
	];
	incompatibleResourcePacks = [];
	lang = "en_gb";
	advancedItemTooltips = true;
	tutorialStep = "none";
	skipMultiplayerWarning = true;
	joinedFirstServer = true;
	onboardAccessibility = false;
	"key_key.socialInteractions" = "key.keyboard.unknown";
	"key_key.screenshot" = "key.keyboard.unknown";
	"key_key.saveToolbarActivator" = "key.keyboard.unknown";
	"key_key.loadToolbarActivator" = "key.keyboard.unknown";
	modKeybinds = {
		antighost = [ "key_key.antighost.reveal:key.keyboard.f10" ];
		autofish = [ "key_key.autofish.open_gui:key.keyboard.unknown" ];
		betterCommandBlockUI = [ "key_key.bcbui.areaselectioninput:key.keyboard.unknown" ];
		c_noIdea = [ "key_Toggle:key.keyboard.unknown" ]; #TODO check
		equipmentCompare = [ "key_equipmentcompare.key.showTooltips:key.keyboard.unknown" ];
		f3Keybind = [ "key_key.f3keybind.f3toggle:key.keyboard.unknown" ];
		gammaUtils = [
			"key_key.gamma_utils.gamma_toggle:key.keyboard.unknown"
			"key_key.gamma_utils.increase_gamma:key.keyboard.unknown"
			"key_key.gamma_utils.decrease_gamma:key.keyboard.unknown"
			"key_key.gamma_utils.night_vision_toggle:key.keyboard.f9"
		];
		jade = [
			"key_key.jade.config:key.keyboard.unknown"
			"key_key.jade.show_overlay:key.keyboard.keypad.0"
			"key_key.jade.toggle_liquid:key.keyboard.keypad.1"
			"key_key.jade.narrate:key.keyboard.unknown"
		];
		mcPings = [ "key_mcpings.key.mark-location:key.keyboard.y" ];
		mouseWheelie = [
			"key_key.mousewheelie.sort_inventory:key.keyboard.0"
			"key_key.mousewheelie.drop_modifier:key.keyboard.unknown"
			"key_key.mousewheelie.deposit_modifier:key.keyboard.unknown"
			"key_key.mousewheelie.restock_modifier:key.keyboard.unknown"
		];
		rebindAllTheKeys = [
			"key_rebind_all_the_keys.keybind.reload_chunks:key.keyboard.f"
			"key_rebind_all_the_keys.keybind.clear_chat:key.keyboard.unknown"
			"key_rebind_all_the_keys.keybind.cycle_render_distance:key.keyboard.f25"
			"key_rebind_all_the_keys.keybind.dynamic_texture_dump:key.keyboard.unknown"
		];
		visibleBarriers = [
			"key_key.visiblebarriers.visible:key.keyboard.f6"
			"key_key.visiblebarriers.fullbright:key.keyboard.unknown"
			"key_key.visiblebarriers.zoom:key.keyboard.unknown"
		];
		xaerosMinimap = [
			"key_gui.xaero_instant_waypoint:key.keyboard.n"
			"key_gui.xaero_toggle_map:key.keyboard.backslash"
			"key_gui.xaero_enlarge_map:key.keyboard.unknown"
			"key_gui.xaero_waypoints_key:key.keyboard.comma"
			"key_gui.xaero_zoom_in:key.keyboard.equal"
			"key_gui.xaero_zoom_out:key.keyboard.minus"
			"key_gui.xaero_new_waypoint:key.keyboard.unknown"
			"key_gui.xaero_minimap_settings:key.keyboard.unknown"
			"key_gui.xaero_open_settings:key.keyboard.unknown"
			"key_gui.xaero_quick_confirm:key.keyboard.unknown"
		];
		zoomify = [
			"key_zoomify.key.zoom:key.keyboard.z"
			"key_zoomify.key.zoom.secondary:key.keyboard.unknown"
		];
		zz_craftPresence = [
			"key_key.craftpresence.config_keycode.name:key.keyboard.unknown"
		];
	};
	soundCategory_music = 0.0;
}
