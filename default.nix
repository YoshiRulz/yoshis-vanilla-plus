{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/23.11.tar.gz") {
	overlays = [ (import overlays/build-modrinth-pack.nix) (import overlays/minecraft-mods) ];
}
}: let
	packs = pkgs.callPackage ./packs.nix {};
in packs // packs.pure
