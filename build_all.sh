#!/bin/sh
cd "$(dirname "$0")"
nix-build --pure -A purer
nix-build --pure -A pure
NIXPKGS_ALLOW_UNFREE=1 nix-build --pure -A impure
NIXPKGS_ALLOW_UNFREE=1 nix-build --pure -A yoshi
