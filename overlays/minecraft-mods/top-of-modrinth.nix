{ lib, fetchMetadataFromModrinth }: let
	inherit (lib) licenses;
in final: {
	# the 500 most downloaded Fabric mods on Modrinth, attr names matching slugs of project pages:
	"3dskinlayers".latestStable = fetchMetadataFromModrinth {
		modSlug = "3dskinlayers";
		versionID = "kJmEO0xO";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "4209b7c1f768202b2f70ec5ab499b26ae9d4e73cf1e855099fe5fb23db0512e5";
		meta = {
			description = "Fixes the \"hat\" layer of players' skins (by adding sides) so they don't appear to be floating";
			license = licenses.tr7zw; # https://github.com/tr7zw/3d-Skin-Layers/blob/1.6.2/LICENSE
			modID = "skinlayers3d";
		};
	};
	#accelerated-decay
	#ad-astra
	adaptive-tooltips.latestStable = fetchMetadataFromModrinth {
		modSlug = "adaptive-tooltips";
		versionID = "MgbaLEPy";
		sha256 = "32d8577e634fc4084a8e3bcd1171ed1921580a020efeea2f52fe8b4a8580cade";
		meta = {
			description = "Improves item and button tooltip placement, especially for modded items with lots of flavourtext";
			license = licenses.lgpl3Only; # https://github.com/isXander/AdaptiveTooltips/blob/1.3.0/LICENSE
		};
	};
	#additional-structures
	#adorn
	#advanced-netherite
	#advancement-plaques
	#advancementinfo
	#advancements-debug
	#adventurez
	#ae2
	#alloy-forgery
	alternate-current.latestStable = fetchMetadataFromModrinth {
		modSlug = "alternate-current";
		versionID = "zrRsTCOk";
		side = "both";
		sha256 = "a54e4390b9359fa2503c611f6d0228d5cb98d678dd4b8e5a74577d914e0aa412";
		meta = {
			description = "Optimises power/signal propagation through redstone dust lines";
			license = licenses.mit; # https://github.com/SpaceWalkerRS/alternate-current/blob/v1.7/LICENSE
		};
	};
	#ambientsounds
	#amecs
	#animal_feeding_trough
	#animatica
	#another-furniture
	antighost.latestStable = fetchMetadataFromModrinth {
		modSlug = "antighost";
		versionID = "qrjEOkM4";
		sha256 = "59f11097c3005d5efcec93d0cd8a1ce690dc1ed65b9e56c2b1ed7bdca24cf022";
		meta = {
			description = "Adds a keybind to re-request nearby block data from the server for when the client becomes desynced (\"ghost blocks\")";
			license = licenses.mit; # https://github.com/gbl/AntiGhost/blob/3767ea6fa499568df4d3da1f1f428b5f6678ec48/LICENSE
		};
	};
	appleskin.latestStable = fetchMetadataFromModrinth {
		modSlug = "appleskin";
		versionID = "pmFyu3Sz";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "e256b55ec94408fd32ba3963fb21cd54718a087134ed24ea2c4c660f25bae977";
		meta = {
			description = "Improves hunger-related HUD and adds hunger points to consumables' tooltips";
			license = licenses.unlicense; # https://github.com/squeek502/AppleSkin/blob/v2.5.1/LICENSE
		};
	};
	#aquamirae
	architectury-api.latestStable = fetchMetadataFromModrinth {
		modSlug = "architectury-api";
		versionID = "ueVsvU6j";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "54c92a1d353ca55a4ecb8d21c4b1fdb8f354b434a0c9448b523237cec9de1d7a";
		meta = {
			license = licenses.lgpl3Only; # https://github.com/architectury/architectury-api/blob/93c804fe070f34998df3c73086620c0cc42b4049/LICENSE.md
			modID = "architectury";
		};
	};
	#areas
	#artifacts
	#athena-ctm
	#attributefix
	#audio-extension-for-fancymenu
	auth-me.latestStable = fetchMetadataFromModrinth {
		modSlug = "auth-me";
		versionID = "HhWVxCH0";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "d8c4f0d87acda52c94a6f341cd98db22804e934b322eda4470089e3a511e8070";
		meta = {
			description = "Allows re-authenticating from within the game";
			license = licenses.mit; # https://github.com/axieum/authme/blob/v8.0.0%2B1.20.4/LICENCE.txt
			modID = "authme";
		};
	};
	#auto-third-person
	#autotag
	#auudio
	#azurelib
	#azurelib-armor
	#bad-wither-no-cookie
	badoptimizations.latestStable = fetchMetadataFromModrinth {
		modSlug = "badoptimizations";
		versionID = "Go4FoZir";
		sha256 = "ceb34693a11e468360e54af82dd812c1c5837be1bf83f12b03a2366eadf04fab";
		meta = {
			description = "Skips some idempotent calls in the pre-rendering chain, among other things";
			license = licenses.mit; # https://github.com/ItsThosea/BadOptimizations/blob/cf6c12acc56873a462ebd8682c4c368498eb3f60/LICENSE
		};
	};
	#badpackets
	balm.latestStable = fetchMetadataFromModrinth {
		modSlug = "balm";
		versionID = "VhfHxcat";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "5d790c79d579bbebbc3cb5c9656cd1b3b361a18b364492821585a42ee07f652c";
		meta = {
			license = licenses.twelveIterations; # https://github.com/TwelveIterationMods/Balm/blob/v9.0.5/LICENSE
			modID = "balm-fabric";
		};
	};
	#bartering-station
	#bclib
	#beautify-refabricated
	#bedrockify
	#bedrockwaters
	#better-advancements
	#better-animations-collection
	#better-archeology
	#better-beds
	#better-biome-blend
	#better-clouds
	#better-combat
	better-enchanted-books.latestStable = fetchMetadataFromModrinth {
		modSlug = "better-enchanted-books";
		versionID = "lCF0kobd";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "c397d1948f769a84f6271b64275c5bf2bb88dc9dae31a251c6e4f7c4d13cb3d9";
		meta = {
			description = "Colours enchanted books so you can tell enchantments apart";
			license = licenses.unlicense; # https://github.com/Bernasss12/BetterEnchantedBooks/blob/5513c3fb3924f5495358b328da82caf5fa3de333/LICENSE
			modID = "bebooks";
		};
	};
	better-end-sky.latestStable = fetchMetadataFromModrinth {
		modSlug = "better-end-sky";
		versionID = "7Hl03yPV";
		sha256 = "9d5eb5ff51dfc7123dfea318a027ec5fdf96ac9a29451367ce36cfcb88833679";
		meta = {
			description = "Replaces the End's skybox with the one from the Better End mod";
			license = licenses.mit; # https://github.com/PinkGoosik/better-end-sky/blob/3fb7b44e57d4f62a53c8ec37cbc4fda8eedcf15d/src/main/resources/fabric.mod.json#L18
			modID = "better_end_sky";
		};
	};
	better-mount-hud.latestStable = fetchMetadataFromModrinth {
		modSlug = "better-mount-hud";
		versionID = "h1QpxElt";
		sha256 = "961a423cc427d4c785354239e8e7b251b5575756b1cf85dd1bc85011b47f0fbf";
		meta = {
			description = "Re-enables food and XP HUDs that are hidden when mounted";
			license = licenses.gpl3Only; # https://github.com/Lortseam/better-mount-hud/blob/3ea29431d19432f6eff1e255b3fa521c3f0218d9/LICENSE
			modID = "bettermounthud";
		};
	};
	#better-ping-display-fabric
	#better-stats
	#better-than-mending
	#better-third-person
	#better-tridents
	#betterend
	#betterf3
	#betterhurtcam
	#betternether
	#biomes-o-plenty
	#biomesyougo
	#bisect-mod
	#blur-fabric
	boat-item-view.latestStable = fetchMetadataFromModrinth {
		modSlug = "boat-item-view";
		versionID = "Q3Z6GESL";
		sha256 = "45ef79853a6e968323dadc477f2b68c046948a9e5b2445fed3ab5586598347cf";
		meta = {
			description = "Re-enables rendering the held item while rowing a boat";
			license = licenses.lgpl3Only; # https://github.com/50ap5ud5/BoatItemView/blob/15da71f70667bae6fb0d98f84add9be61cbf749e/LICENSE
			modID = "boatiview";
		};
	};
	bobby.latestStable = fetchMetadataFromModrinth {
		modSlug = "bobby";
		versionID = "jGGumR4a";
		sha256 = "82b20a5c104de33dc01364ff2bf1354132327d085a3dc10954b5ea0ff3d53f51";
		meta = {
			description = "Caches chunks that fall out of render distance, and allows increasing the max. render distance";
			license = licenses.lgpl3Plus; # https://github.com/Johni0702/bobby/blob/v5.0.3/LICENSE.LESSER.md
		};
	};
	bookshelf-lib.latestStable = fetchMetadataFromModrinth {
		modSlug = "bookshelf-lib";
		versionID = "96zaataP";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "40fd8a56d3fd77959645c3d86005a33cf2a9d1b42ae7f4c03a9ec78a06a123b5";
		meta = {
			license = licenses.lgpl21Only; # https://github.com/Darkhax-Minecraft/Bookshelf/blob/85c5de00cff2f28cd3cfc5c0948f9bc79209e8f8/License.md
			modID = "bookshelf";
		};
	};
	#boosted-brightness
	#borderless-mining
	#bosses-of-mass-destruction
	#botania
	#botarium
	#bountiful
	#c2me-fabric
	#caelus
	#camera-utils
	#capes
	#cardinal-components-api
	#carpet
	#carry-on
	#cave-dust
	#cc-tweaked
	#cem
	#charm-of-undying
	#charmonium
	chat-heads.latestStable = fetchMetadataFromModrinth {
		modSlug = "chat-heads";
		versionID = "gqvNiLkw";
		sha256 = "b8df5a6ae8c4f2c5492a9235463aa3f231af27964d7e240fcba1421c99e89736";
		meta = {
			description = "Displays players' faces in chat to match the player list";
			license = licenses.mpl20; # https://github.com/dzwdz/chat_heads/blob/0.10.31/LICENSE
			modID = "chat_heads";
		};
	};
	chatpatches.latestStable = fetchMetadataFromModrinth {
		modSlug = "chatpatches";
		versionID = "TC2Pejkl";
		packwizDeps = [ final.fabric-api.latestStable final.yacl.latestStable ];
		sha256 = "c20b67c03bc31c5bd924dcd1bcea4f3078f5a291855fc22f7862cfb7cca0dfc2";
		meta = {
			description = "Buffs the in-game chat scrollback buffer, among other things";
			license = licenses.lgpl3Only; # https://github.com/mrbuilder1961/ChatPatches/blob/f5d00a807092627f60bc7ce31aab1b4ec0bb7991/LICENSE
		};
	};
	#chefs-delight
	cherished-worlds.latestStable = fetchMetadataFromModrinth {
		modSlug = "cherished-worlds";
		versionID = "zTMYTZJx";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "8af5b9d2a64353cab7ca59ef910f4a097c328ad3f5a941d526da71e094970fa5";
		meta = {
			description = "Allows marking saves as favourites, pinning them to the top of the list";
			license = licenses.lgpl3Plus; # https://github.com/illusivesoulworks/cherishedworlds/blob/f01672877bc3a9833a282377850686397d8c123a/COPYING.LESSER
			modID = "cherishedworlds";
		};
	};
	#chipped
	chunks-fade-in.latestStable = fetchMetadataFromModrinth {
		modSlug = "chunks-fade-in";
		versionID = "PGlH4K9i";
		packwizDeps = [ final.sodium.latestStable ];
		sha256 = "f1cea27f5ae59cf1eb5616a1b80e875f6b5ec7c45da005ba7654dc8977e5eaf1";
		meta = {
			description = "Adds an animation to the initial rendering of chunks";
			license = licenses.gpl3Only; # https://github.com/kerudion/chunksfadein/blob/1e5f7ff6aac4796dd8b8a36dacf7c9e5c666846b/LICENSE
			modID = "chunksfadein";
		};
	};
	#chunky #TODO plugin?
	#cit-resewn
	#cleancut
	cleardespawn.latestStable = fetchMetadataFromModrinth {
		modSlug = "cleardespawn";
		versionID = "jpRIGRtD";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "be484c508ba6866ae478956e053a069643d489b8a92eb6079057d70e3337c106";
		meta = {
			description = "Displays dropped items' lifetimes in-world (as a blinking effect on items close to despawning)";
			license = licenses.mit; # https://github.com/StrikerRockers-Mods/ClearDespawn/blob/1.20.2-1.1.15/LICENSE
		};
	};
	#clickthrough
	client-tweaks.latestStable = fetchMetadataFromModrinth {
		modSlug = "client-tweaks";
		versionID = "ecFWvFOq";
		packwizDeps = [ final.balm.latestStable final.fabric-api.latestStable ];
		sha256 = "c4dc864579fead25dc0f5dc72bf05a457885fb67a75af42bc01e5dfc736d1a8f";
		meta = {
			description = "Various changes to dual-wielding items, and brings back the master and music volume sliders";
			license = licenses.twelveIterations; # https://github.com/TwelveIterationMods/ClientTweaks/blob/v13.0.2/LICENSE
			modID = "clienttweaks";
		};
	};
	cloth-config.latestStable = fetchMetadataFromModrinth {
		modSlug = "cloth-config";
		versionID = "eBZiZ9NS";
		sha256 = "f250d6778f5b8d03ca32913bea5ea677508e9bb6d8201cdebe005bbe9c3100ee";
		meta = {
			license = licenses.lgpl3Only; # https://github.com/shedaniel/cloth-config/blob/bc60b2f30c84f7f4df580092bd76017d48bd7abb/LICENSE.md
		};
	};
	clumps.latestStable = fetchMetadataFromModrinth {
		modSlug = "clumps";
		versionID = "jdeTwq6v";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "1bfa978c09e27a1b064119a8ebe53083f63abc3625055f49f0f086a64c0222bb";
		meta = {
			description = "Combines XP orb entities";
			license = licenses.mit; # https://github.com/jaredlll08/Clumps/blob/6f56ba314adf695e0ef34f1a4d4084b9c43c1ac6/LICENSE.md
		};
	};
	#cobblemon
	#collective
	#colormatic
	#combat-roll
	#comforts
	completeconfig.latestStable = fetchMetadataFromModrinth {
		modSlug = "completeconfig";
		versionID = "lvqqK55F";
		sha256 = "debab1b6732fce31b240d311d86b441329ba46aae132c9f236af1fe90af00d94";
		meta = {
			license = licenses.asl20; # https://github.com/Lortseam/completeconfig/blob/2.5.3/LICENSE
		};
	};
	#connectible_chains
	#continuity
	#controlify
	controlling.latestStable = fetchMetadataFromModrinth {
		modSlug = "controlling";
		versionID = "jM1vTyg9";
		packwizDeps = [ final.fabric-api.latestStable final.searchables.latestStable ];
		sha256 = "a659fc922dfbcbb29bad2bbc11dd34cb1686c3c9cde9a93fe484db0f4e4c3062";
		meta = {
			description = "Adds a search box to the keybinds menu";
			license = licenses.mit; # https://github.com/jaredlll08/Controlling/blob/76887d7bf46ece88635ea0d1a283eda0bf1fc112/LICENSE
		};
	};
	#corgilib
	#cosmetic-armor
	#craftify
	#crafting-tweaks
	craftpresence.latestStable = fetchMetadataFromModrinth {
		modSlug = "craftpresence";
		versionID = "V0ooGFd0";
		sha256 = "21d5a2dc79b4f21bed9f7f773b1525f7c7875e2e06e1e11f01cc1d22bff20a63";
		meta = {
			description = "Adds \"rich presence\" for Discord";
			license = licenses.mit; # https://gitlab.com/CDAGaming/CraftPresence/-/blob/release/v2.3.0/LICENSE
			# + uses 3p services
		};
	};
	#craterlib
	#crawl
	#create-enchantment-industry-fabric
	#create-fabric
	#create-fabric-sodium-fix
	#create-goggles
	#create-steam-n-rails
	#create-structures #TODO datapack?
	#createaddition
	creativecore.latestStable = fetchMetadataFromModrinth {
		modSlug = "creativecore";
		versionID = "kIfhbwdL";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "0b3ff7374dbc2b371ef6d535cddd4871171b11a07a59a57c5cf1175ba9acb941";
		meta = {
			license = licenses.lgpl3Only; # https://github.com/CreativeMD/CreativeCore/blob/7489c241c8b510f2f482d5b5c804879e30852b3a/LICENSE
		};
	};
	#creeper-overhaul
	#cristel-lib
	#ct-overhaul-village
	#ctov-farmers-delight-compat #TODO datapack?
	#ctov-friends-and-foes-compat #TODO datapack?
	#ctrl-q
	#cull-leaves
	#cull-less-leaves
	#curios
	#customskinloader
	#dark-loading-screen
	#dashloader
	#dawn
	#dcwa
	debugify.latestStable = fetchMetadataFromModrinth {
		modSlug = "debugify";
		versionID = "gGTvKKDk";
		packwizDeps = [ final.yacl.latestStable ];
		sha256 = "60d43521b68e4639ad7e258a8d10759e70a22e543aa0050727283a848466d0b9";
		meta = {
			description = "Implements obvious fixes for dozens of bugs that Mojang is too lazy to fix, and disables telemetry";
			license = licenses.lgpl3Only; # https://github.com/isXander/Debugify/blob/1.20.4%2B1.0/LICENSE
		};
	};
	#decorative-blocks-fork
	#deeperdarker
	#default-options
	#detail-armor-bar
	#diagonal-fences
	#dismount-entity
	#distanthorizons
	#distinguished-potions
	#do-a-barrel-roll
	#do-api
	#double-doors
	#drippy-loading-screen
	#dripsounds-fabric
	#dungeons-and-taverns #TODO datapack?
	#durability-tooltip
	#durabilityviewer
	dynamic-fps.latestStable = fetchMetadataFromModrinth {
		modSlug = "dynamic-fps";
		versionID = "6ZZnpiKt";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "17cb1c44cbc5486f3eedf1e4de4c48c29a4f8cd8282ff757e64c0b6f3b964dd6";
		meta = {
			description = "Adds a separate framerate limit for when Minecraft is unfocused";
			license = licenses.mit; # https://github.com/juliand665/Dynamic-FPS/blob/3.3.3/LICENSE
			modID = "dynamic_fps";
		};
	};
	#dynamiccrosshair
	#dynamiccrosshaircompat
	#dynamicsurroundings_remasteredfabric
	e4mc.latestStable = fetchMetadataFromModrinth {
		modSlug = "e4mc";
		versionID = "vVFlVRt7";
		side = "both";
		packwizDeps = [ final.fabric-api.latestStable final.fabric-language-kotlin.latestStable ];
		sha256 = "99b1060fa083ca355f35f09fe02ef04832a313cff855de8522fd1766164e0082";
		meta = {
			description = "Enhances \"Open to LAN\" with a reverse proxy (https://e4mc.link) so your server can be accessed from anywhere, even if you're behind a firewall/NAT/whatever";
			license = licenses.mit; # https://git.skye.vg/me/e4mc_minecraft/src/commit/ccb7025b74811f05b1de49cad3dc0960a0d3edba/LICENSE
			modID = "e4mc_minecraft";
		};
	};
	#easy-anvils
	#easy-magic
	#eating-animation
	ebe.latestStable = fetchMetadataFromModrinth {
		modSlug = "ebe";
		versionID = "eIFo7wvq";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "179d1b7ca85bd3202749e2cc95413daec331dd37dbcf66d07f12a2cd1c0ee397";
		meta = {
			description = "Uses block models instead of the slower entity models for block entities when they're not animating";
			license = licenses.lgpl3Only; # https://github.com/FoundationGames/EnhancedBlockEntities/blob/5c85fee987369595ff576238c40e8986306c3557/LICENSE.txt
			modID = "enhancedblockentities";
		};
	};
	#edf-remastered #TODO datapack?
	#elytra-slot
	#elytra-trims
	#embeddium
	emi.latestStable = fetchMetadataFromModrinth {
		modSlug = "emi";
		versionID = "PABSe7Zo";
		sha256 = "1f071cc75fb7052ddd387aabb4a308ae1590ac60d896f6f2286727834d008d9b";
		meta = {
			description = "Adds a superpowered version of the recipe book, with an index of every item in the game and their recipes";
			license = licenses.mit; # https://github.com/emilyploszaj/emi/blob/1.1.0%2B1.20.4/LICENSE
		};
	};
	#emi-loot
	#emiffect
	emitrades.latestStable = fetchMetadataFromModrinth {
		modSlug = "emitrades";
		versionID = "ihis4OcC";
		sha256 = "28e274fb3b75d8e2604eab07f9ea6e9d663fa8a24aeafce09bdbfe5d3a88fac5";
		meta = {
			description = "Villager trade plugin for EMI";
			license = licenses.mit; # https://github.com/Prismwork/EMITrades/blob/21fbf13c1ceeeae0da82ba9f4a6cbd6cde40aa68/LICENSE
		};
	};
	#emoji-type
	#emotecraft
	enchantment-descriptions.latestStable = fetchMetadataFromModrinth {
		modSlug = "enchantment-descriptions";
		versionID = "yltXabOE";
		packwizDeps = [ final.bookshelf-lib.latestStable final.fabric-api.latestStable ];
		sha256 = "960d7d9db2d42e26d40ffd59f135969c3a1f86208eedc97ba6cf4ec510b42406";
		meta = {
			description = "Includes a description of each enchantment in the tooltips of enchanted books";
			license = licenses.lgpl21Only; # https://github.com/Darkhax-Minecraft/Enchantment-Descriptions/blob/11c5c74e7e0ae6b0089be4119c163189e4481a29/gradle.properties#L30
			modID = "enchdesc";
		};
	};
	#enderman-overhaul
	#endrem
	enhanced-attack-indicator.latestStable = fetchMetadataFromModrinth {
		modSlug = "enhanced-attack-indicator";
		versionID = "QTzKBcJj";
		sha256 = "e13e1480c8a55003cdc5047505744361b8d42dfd43e021a21790c6309c904880";
		meta = {
			description = "Adds more functionality to the attack indicator";
			license = licenses.mit; # https://github.com/Minenash/Enhanced-Attack-Indicator/blob/v1.0.4%2B1.20.3/LICENSE
			modID = "enhanced_attack_indicator";
		};
	};
	#enhancedvisuals
	#entity-collision-fps-fix
	#entity-model-features
	#entity-view-distance
	entityculling.latestStable = fetchMetadataFromModrinth {
		modSlug = "entityculling";
		versionID = "HSirwtwV";
		sha256 = "d8141ee9ad948865c4da8594fe1e785ff6933d3f5b299234d002e2014559c0e4";
		meta = {
			description = "Skips rendering entities that would be obscured by a wall";
			license = licenses.tr7zw; # https://github.com/tr7zw/EntityCulling/blob/1.6.3.1-1.20.4/LICENSE-EntityCulling
		};
	};
	#entitytexturefeatures
	equipment-compare.latestStable = fetchMetadataFromModrinth {
		modSlug = "equipment-compare";
		versionID = "YpXiHOSx";
		packwizDeps = [ final.iceberg.latestStable ];
		sha256 = "4c49e8056cce4128d8b1296ada37d17a682918ac090f6fb4d5f898020650e02c";
		meta = {
			description = "Shows a tooltip for the currently-equipped armour when looking at the tooltip for other armour";
			license = licenses.cc-by-nc-nd-40; # https://github.com/AHilyard/EquipmentCompare/blob/e95dbfa89b0cfbc155ff65929c2dce8788aa992d/LICENSE
			modID = "equipmentcompare";
		};
	};
	#essential
	#evergreenhud
	#every-compat
	exordium.latestStable = fetchMetadataFromModrinth {
		modSlug = "exordium";
		versionID = "WyhKYeMf";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "350e3aa46726c4ce33c9c383c673ab131c76c9015b5aa8c5532521beed1dfca4";
		meta = {
			description = "Renders GUI at a lower framerate than the game world";
			license = licenses.tr7zw; # https://github.com/tr7zw/Exordium/blob/1.2.1-1.20.4/LICENSE
		};
	};
	#expanded-delight
	#explorations
	#explorers-compass
	#explorify #TODO datapack?
	#explosive-enhancement
	#extended-drawers
	#extra-mod-integrations
	#extrasounds
	#extreme_sound_muffler
	fabric-api.latestStable = fetchMetadataFromModrinth {
		modSlug = "fabric-api";
		versionID = "UgdmocrA";
		sha256 = "5565745a71c49c243d26050d2dc94b7544057ff1e39b24428f9c3ca33df1c828";
		meta = {
			license = licenses.asl20; # https://github.com/FabricMC/fabric/blob/0.95.3%2B1.20.4/LICENSE
		};
	};
	fabric-language-kotlin.latestStable = fetchMetadataFromModrinth {
		modSlug = "fabric-language-kotlin";
		versionID = "vlhvI5Li";
		sha256 = "a856296531bab559c7dbe061827078e206b9cf194be3be618c462c62ab4fd841";
		meta = {
			license = licenses.asl20; # https://github.com/FabricMC/fabric-language-kotlin/blob/1.10.17%2Bkotlin.1.9.22/LICENSE
		};
	};
	#fabric-seasons
	#fabric-seasons-delight-compat
	#fabric-seasons-extras
	#fabricskyboxes
	#fabricskyboxes-interop
	#fabrishot
	#fadeless
	fallingleaves.latestStable = fetchMetadataFromModrinth {
		modSlug = "fallingleaves";
		versionID = "KpvXCE2X";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "0c4c20d601b15dbc85c275732d9efe9369597503c7d6caa9a7fee217f67073e7";
		meta = {
			description = "Adds leaf particles which spawn from leaves";
			license = licenses.mit; # https://github.com/RandomMcSomethin/fallingleaves/blob/a3d5ee0ff6dcf912cfee33478b607b9857a1f982/LICENSE
		};
	};
	#fallingtree
	#fancymenu
	#farmers-delight-fabric
	#farmers-knives
	#fast-paintings
	#fastanim
	#fastload
	fastquit.latestStable = fetchMetadataFromModrinth {
		modSlug = "fastquit";
		versionID = "vOAKK0JB";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "ff278c8a9a5e56d412fb2e7106790b0ac33b0a4a8699978f0ae4d5a2c70c63b3";
		meta = {
			description = "Moves the work of closing a local world to the background";
			license = licenses.mit; # https://github.com/KingContaria/FastQuit/blob/126727c747d661d38554059d3c3d7d8670f8fbed/LICENSE
		};
	};
	ferrite-core.latestStable = fetchMetadataFromModrinth {
		modSlug = "ferrite-core";
		versionID = "pguEMpy9";
		sha256 = "4146027b92b4885fddec00435233c61c3728b5e09265982ab187021519a4fa89";
		meta = {
			description = "Reduces RAM consumption by improving a few low-hanging fruit";
			license = licenses.mit; # https://github.com/malte0811/FerriteCore/blob/build-6.0.3/LICENSE
			modID = "ferritecore";
		};
	};
	#feytweaks
	#first-person-model
	#fish-of-thieves
	#forcecloseworldloadingscreen
	forge-config-api-port.latestStable = fetchMetadataFromModrinth {
		modSlug = "forge-config-api-port";
		versionID = "xbVGsTLe";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "d1e32ea50e119b059ffe57b2785625679cf04641dfa2d7189197f09eb63aa7f7";
		meta = {
			license = licenses.mpl20; # https://github.com/Fuzss/forgeconfigapiport/blob/ca7e68fc19734be125d0abec98b625f86e1d3624/LICENSE.md
			modID = "forgeconfigapiport";
		};
	};
	#forge-config-screens
	#forgetmechunk
	#fps-reducer
	#fpsdisplay
	freecam.latestStable = fetchMetadataFromModrinth {
		modSlug = "freecam";
		versionID = "MrgEk6HV";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "170ea395764e635c34c8d44aff02447a44aca3006c6591be355482041e437d6b";
		meta = {
			description = "Allows moving the camera around to see far-away things, like spectator mode, but without moving the player as well";
			license = licenses.mit; # https://github.com/MinecraftFreecam/Freecam/blob/1.2.3/LICENSE
		};
	};
	#friends-and-foes
	#from-the-fog #TODO datapack?
	#full-brightness-toggle
	#fusion-connected-textures
	#fwaystones
	gamma-utils.latestStable = fetchMetadataFromModrinth {
		modSlug = "gamma-utils";
		versionID = "jVlG1ygl";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "e2f157f2434341292e705022f597411ba2be3a86f1f7cf7a24a3c759123043b8";
		meta = {
			description = "Adds a toggleable 'fullbright' mode, which allows you to see in the dark";
			license = licenses.lgpl3Only; # https://github.com/Sjouwer/gamma-utils/blob/5fdd7d4c8839d4c08db564e6ea99f576e5537672/LICENSE
			modID = "gammautils";
		};
	};
	#geckolib
	#geophilic #TODO datapack?
	#globalpacks
	#handcrafted
	#held-item-info
	#heracles
	highlight.latestStable = fetchMetadataFromModrinth {
		modSlug = "highlight";
		versionID = "Dun8GvAw";
		sha256 = "ba2392026d38355372006ed36ba6e43bd439a5094d74fd1e232098884d2e5d08";
		meta = {
			description = "Fixes block selection reticles for lecterns, signs, etc.";
			license = licenses.mit; # https://github.com/Team-Resourceful/Highlight/blob/v2.2.0/LICENSE.md
		};
	};
	#hold-that-chunk
	#i18nupdatemod
	iceberg.latestStable = fetchMetadataFromModrinth {
		modSlug = "iceberg";
		versionID = "ZioCfzuX";
		sha256 = "b88c6c427f6a4cd5160db8590d9dfc3d1613e7a284e38c451848af14d52d5086";
		meta = {
			license = licenses.cc-by-nc-nd-40; # https://github.com/AHilyard/Iceberg/blob/f8f91c8a242fe9ad534575fe715c57217a0324be/LICENSE
		};
	};
	#illager-invasion
	immediatelyfast.latestStable = fetchMetadataFromModrinth {
		modSlug = "immediatelyfast";
		versionID = "59UuNTrz";
		sha256 = "b411b04e1dace15c7fc1465bb161c8e621750b1577282e0918f956212a57e864";
		meta = {
			description = "Optimises batching of low-level graphics calls, among other things";
			license = licenses.lgpl3Only; # https://github.com/RaphiMC/ImmediatelyFast/blob/v1.2.8/LICENSE
		};
	};
	#immersive-aircraft
	#immersive-armors
	#immersive-melodies
	#immersive-paintings
	#immersiveportals
	#in-game-account-switcher
	#incendium
	indium.latestStable = fetchMetadataFromModrinth {
		modSlug = "indium";
		versionID = "Aouse6P7";
		packwizDeps = [ final.iceberg.latestStable final.sodium.latestStable ];
		sha256 = "e416b18eebf514dea2014cf5c7fd3ee8493934a0b4e937509f15172ad271fde2";
		meta = {
			license = licenses.asl20; # https://github.com/comp500/Indium/blob/1.0.28%2Bmc1.20.4/LICENSE
		};
	};
	#inmis
	#inmisaddon
	#interactic
	#inventory-profiles-next
	#inventory-sorting
	#invmove
	#iris
	#item-borders
	item-highlighter.latestStable = fetchMetadataFromModrinth {
		modSlug = "item-highlighter";
		versionID = "IQhlGx7m";
		packwizDeps = [ final.forge-config-api-port.latestStable final.iceberg.latestStable ];
		sha256 = "1bc74743327f85844df73f45f973fb5b394cd4abfef2ee8d0ca6383fc0d92c0b";
		meta = {
			description = "Highlights items which were recently picked up in the inventory";
			license = licenses.cc-by-nc-nd-40; # https://github.com/AHilyard/Highlighter/blob/d20868c623e6d3178df9c85aadb5593aee529a5a/LICENSE
			modID = "highlighter";
		};
	};
	#item-model-fix
	#itemswapper #TODO plugin?
	jade.latestStable = fetchMetadataFromModrinth {
		modSlug = "jade";
		versionID = "fNHCa6bl";
		sha256 = "919e0f9bdf11b4960591cef1f0cb7fe6bb8bbf21135b423c0d014b00b4a06cb8";
		meta = {
			description = "Adds a block inspection widget to the HUD";
			license = licenses.cc-by-nc-sa-40; # https://github.com/Snownee/Jade/blob/fabric-13.2.1/LICENSE.md
		};
	};
	#jade-addons-fabric
	#jamlib
	#jei
	#journeymap
	#just-enough-professions-jep
	#just-enough-resources-jer
	#kambrik
	#kiwi
	kleeslabs.latestStable = fetchMetadataFromModrinth {
		modSlug = "kleeslabs";
		versionID = "kBG7QKqK";
		side = "both";
		packwizDeps = [ final.balm.latestStable final.fabric-api.latestStable ];
		sha256 = "9ed846beaf233985c5ea20a9a05a5b5b12cadefc38ecc81630c824f3d5cc66f1";
		meta = {
			description = "Only breaks one slab of a double-slab unless sneaking";
			license = licenses.twelveIterations; # https://github.com/TwelveIterationMods/KleeSlabs/blob/v16.0.1/LICENSE
		};
	};
	#konkrete
	krypton.latestStable = fetchMetadataFromModrinth {
		modSlug = "krypton";
		versionID = "bRcuOnao";
		sha256 = "fe6abe1ea33393fe1441801f79eda74ea892e5f4b677a56eccafafcbf4d253ca";
		meta = {
			description = "Replaces large portions of the networking stack with more performant code";
			license = licenses.lgpl3Only; # https://github.com/astei/krypton/blob/v0.2.6/LICENSE
		};
	};
	#ksyxis
	#lambdabettergrass
	lambdynamiclights.latestStable = fetchMetadataFromModrinth {
		modSlug = "lambdynamiclights";
		versionID = "mrQ8ZiyU";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "d398662ae6e9d87e89384ba50a9787dbeb9425902a18b0cd8239efd0b966b453";
		meta = {
			description = "Makes glowing entities, entities holding a glowing block, and dropped glowing blocks illuminate their surroundings";
			license = licenses.mit; # https://github.com/LambdAurora/LambDynamicLights/blob/v2.3.4%2B1.20.4/LICENSE
			modID = "lambdynlights";
		};
	};
	language-reload.latestStable = fetchMetadataFromModrinth {
		modSlug = "language-reload";
		versionID = "SSvudGpI";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "037f6b72bde7b586901f261820197efb52a369cdad3557eeaaa7a3f8e9c19799";
		meta = {
			description = "Replaces the single language option with a preference list";
			license = licenses.mit; # https://github.com/Jerozgen/LanguageReload/blob/1.5.10%2B1.20.3/LICENSE
			modID = "languagereload";
		};
	};
	#lazy-language-loader
	#lazydfu
	#leave-my-bars-alone
	#leaves-be-gone
	#legendary-tooltips
	#lets-do-vinery
	#libipn
	#libz
	#lilac
	#litematica-printer
	lithium.latestStable = fetchMetadataFromModrinth {
		modSlug = "lithium";
		versionID = "nMhjKWVE";
		sha256 = "3dedde5213176d72e1c353877fa44315945e8f3da2ba05f5679d81e9f558bb1d";
		meta = {
			description = "Replaces some of Minecraft's physics/gameplay routines with more efficient implementations";
			license = licenses.lgpl3Only; # https://github.com/CaffeineMC/lithium-fabric/blob/mc1.20.4-0.12.1/LICENSE.txt
		};
	};
	lmd.latestStable = fetchMetadataFromModrinth {
		modSlug = "lmd";
		versionID = "qpC0XlcW";
		side = "both";
		sha256 = "df3bf405458a14ae114177c33956530f4fe98b2962b9446e9b7955c5e09dee17";
		meta = {
			description = "Fixes mobs not naturally despawning because they've picked up an item";
			downloadPage = "https://modrinth.com/plugin/lmd";
			license = licenses.lgpl3Only; # https://github.com/frikinjay/let-me-despawn/blob/c58a2a8048ceac628ab9d368348f396108567ce6/LICENSE
			modID = "letmedespawn";
		};
	};
	#load-my-resources
	#log-begone
	#log-cleaner
	#logical-zoom
	#longer-chat-history
	#lootr
	#luna
	#macos-input-fixes
	#magnum-torch
	#main-menu-credits
	make_bubbles_pop.latestStable = fetchMetadataFromModrinth {
		modSlug = "make_bubbles_pop";
		versionID = "ZQx9k5Sj";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "ac2ca5caef9ddb4fd53c035bfaf07026f12bcdb162d19c2fa473bdec8fbec630";
		meta = {
			description = "Improves the air bubble particle mechanic";
			license = licenses.tschipcraft; # https://github.com/Tschipcraft/make_bubbles_pop/blob/0.2.0/LICENSE
		};
	};
	malilib.latestStable = fetchMetadataFromModrinth {
		modSlug = "malilib";
		versionID = "kZJWQDi6";
		sha256 = "a60b40bf13dbb92bb17d64eb25bfd383b7e10eaadca1d8d1364bc06d72d92465";
		meta = {
			license = licenses.lgpl3Only; # https://github.com/maruohon/malilib/blob/4991ed86ac8a92a00fa16117a8ae39dc74ce6bb9/LICENSE.txt
		};
	};
	#max-health-fix
	#mcwifipnp
	memoryleakfix.latestStable = fetchMetadataFromModrinth {
		modSlug = "memoryleakfix";
		versionID = "5xvCCRjJ";
		sha256 = "3c0945a312aca06a2185b00cb03ae482949b008d72614497148aa2df301af019";
		meta = {
			description = "Fixes a couple of particularly bad RAM usage bugs which Mojang hasn't gotten around to yet";
			license = licenses.lgpl21Only; # https://github.com/FxMorin/MemoryLeakFix/blob/v1.1.5/LICENSE
		};
	};
	#merchant-markers
	#mes-moogs-end-structures
	#midnightcontrols
	midnightlib.latestStable = fetchMetadataFromModrinth {
		modSlug = "midnightlib";
		versionID = "EQNuBJf5";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "bd6d19a7233f79f0dc0330f68e20a9e2cc2baeb6c7d0f0a54f97d95a7c2020f7";
		meta = {
			license = licenses.mit; # https://github.com/TeamMidnightDust/MidnightLib/blob/2ff92526ba4be513a0a535e7ffb4171a9837568e/LICENSE
		};
	};
	#mindful-darkness
	#minecraft-comes-alive-reborn
	#minecraft-transit-railway
	#mixin-conflict-helper
	#mixintrace
	#mmmmmmmmmmmm
	modelfix.latestStable = fetchMetadataFromModrinth {
		modSlug = "modelfix";
		versionID = "r6uVoUxU";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "6ff047f3b0e73e22bf5ad0d6751f82cd6f6a4e164a911a37839eba667fa85502";
		meta = {
			description = "Implements obvious fix for MC-73186 re: held item rendering, which Mojang is too lazy to fix";
			license = licenses.gpl3Only; # https://github.com/MehVahdJukaar/modelfix-multi/blob/ae478af732199aeae8d405ea50f601bd0bbdedb1/LICENSE.md
		};
	};
	modernfix.latestStable = fetchMetadataFromModrinth {
		modSlug = "modernfix";
		versionID = "Btq1HFzk";
		sha256 = "b88658496a83e1ade03fe955191c379d413db8915f2f8d9641797b6f22865b25";
		meta = {
			description = "Many fixes, including reducing RAM usage and reducing startup time";
			license = licenses.lgpl3Only; # https://github.com/embeddedt/ModernFix/blob/5.12.1%2B1.20.4/LICENSE
		};
	};
	modmenu.latestStable = fetchMetadataFromModrinth {
		modSlug = "modmenu";
		versionID = "sjtVVlsA";
		sha256 = "b61b0ccdc8328d637c0f57cbb25258491b94ae03c89c4194ad6ca8b86579dd28";
		meta = {
			description = "Adds a mod list GUI";
			license = licenses.mit; # https://github.com/TerraformersMC/ModMenu/blob/v9.0.0/LICENSE
		};
	};
	#monsters-in-the-closet
	#moogs-voyager-structures
	#moonlight
	#more-geodes
	#more-mob-variants
	#morechathistory
	moreculling.latestStable = fetchMetadataFromModrinth {
		modSlug = "moreculling";
		versionID = "KpriJ15b";
		packwizDeps = [ final.cloth-config.latestStable ];
		sha256 = "ef9c4bc27256d065df4feaba2bc05883ba22987c41cd48786070d37ae85cc8e1";
		meta = {
			description = "Skips rendering of block faces that are obscured";
			license = licenses.lgpl21Only; # https://github.com/FxMorin/MoreCulling/blob/v0.22.1-1.20.1%2B/LICENSE
		};
	};
	#mouse-tweaks
	mouse-wheelie.latestStable = fetchMetadataFromModrinth {
		modSlug = "mouse-wheelie";
		versionID = "73ivfwq5";
		sha256 = "4c1fed6a8d4681c1e00d93786ec775bede5a77317bcf70df92cee9908d8d6891";
		meta = {
			description = "Adds a few inventory management features";
			license = licenses.asl20; # https://github.com/Siphalor/mouse-wheelie/blob/v1.13.3%2Bmc1.20.4/LICENSE
			modID = "mousewheelie";
		};
	};
	#mru
	#naturalist
	#natures-compass
	#neruina
	#netherportalfix
	#new-nether-chest
	#nicer-skies
	no-chat-reports.latestStable = fetchMetadataFromModrinth {
		modSlug = "no-chat-reports";
		versionID = "tfv6A4l5";
		sha256 = "a045df0a5d03ef89d92ca9318aa42a5a5e7617b37dd38d32b0a7058c0f6f4ef7";
		meta = {
			description = "Disables most of Mojang's player reporting system";
			license = licenses.wtfpl; # https://github.com/Aizistral-Studios/No-Chat-Reports/blob/14150e4a67caf14cd357e631b19799cdde04cc39/LICENSE
			modID = "nochatreports";
		};
	};
	no-resource-pack-warnings.latestStable = fetchMetadataFromModrinth {
		modSlug = "no-resource-pack-warnings";
		versionID = "Cs4frYPf";
		sha256 = "c8df23a6f0bd41d2f562b460e94bb3df7df1932298bf7b5081e43dfd7c270fae";
		meta = {
			description = "Removes the confirmation screen when enabling resource packs made for older versions";
			license = licenses.mit; # https://github.com/SpaceWalkerRS/no-resource-pack-warnings/blob/v1.3.0/LICENSE
		};
	};
	#no-telemetry
	noisium.latestStable = fetchMetadataFromModrinth {
		modSlug = "noisium";
		versionID = "7p5BqRHp";
		side = "both";
		sha256 = "708c3992f04530d2366b570ad6503b97881d039f2d79530c861b8f276b2ec591";
		meta = {
			description = "Optimises generation of new chunks";
			license = licenses.lgpl3Only; # https://github.com/Steveplays28/noisium/blob/v1.0.2%2Bmc1.20.x/LICENSE
		};
	};
	not-enough-animations.latestStable = fetchMetadataFromModrinth {
		modSlug = "not-enough-animations";
		versionID = "ZLjUeuU8";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "249dec365f1a15ef6d1da48910e5fa04c039e95375ce246d5a771941b6b8892c";
		meta = {
			description = "Changes some player animations";
			license = licenses.tr7zw; # https://github.com/tr7zw/NotEnoughAnimations/blob/1.7.0/LICENSE
			modID = "notenoughanimations";
		};
	};
	notenoughcrashes.latestStable = fetchMetadataFromModrinth {
		modSlug = "notenoughcrashes";
		versionID = "k6ct8Dn2";
		sha256 = "7fad014bedc7a93e4d93b7ebd8112628a25b1f6c121878a70c9c2b5781f3c18e";
		meta = {
			description = "Makes some fatal crashes non-fatal (only sending you back to the main menu)";
			license = licenses.mit; # https://github.com/natanfudge/Not-Enough-Crashes/blob/b74d457eadf9f66ac246000376e6fbf4d2baa1c1/LICENSE
		};
	};
	#noxesium
	#nullscape
	nvidium.latestStable = fetchMetadataFromModrinth {
		modSlug = "nvidium";
		versionID = "J2fuM58R";
		packwizDeps = [ final.sodium.latestStable ];
		sha256 = "07dd8775324c01058896b4572e23a1ca1c355186cc501c61fc68e1151c5e177c";
		meta = {
			description = "Hardware-accelerated rendering improvements (only affects Nvidia GPUs, specifically *Turing* or newer chipsets)";
			license = licenses.lgpl3Only; # https://github.com/MCRcortex/nvidium/blob/156eff2e9ed10d6c99e2dbeb8c8c7d6fe2519f24/LICENSE.txt
		};
	};
	#nyfs-spiders
	#obscure-api
	#ok-zoomer
	#open-parties-and-claims
	#optigui
	#origins
	#overflowing-bars
	owo-lib.latestStable = fetchMetadataFromModrinth {
		modSlug = "owo-lib";
		versionID = "CJ29u2GJ";
		sha256 = "f3636b986e4bf3fd2c66e393dca81170247b157b076f02160ff755e17dc792a9";
		meta = {
			license = licenses.mit; # https://github.com/wisp-forest/owo-lib/blob/0.12.5%2B1.20.3/LICENSE
			modID = "owo";
		};
	};
	#packet-fixer
	paginatedadvancements.latestStable = fetchMetadataFromModrinth {
		modSlug = "paginatedadvancements";
		versionID = "SSrN6xCf";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "a0626154090e5879cb832c75cbc98251e246f0142999d496ac0b084f29a2bc02";
		meta = {
			description = "Improves usability of advancement menu, especially when using datapacks/mods";
			license = licenses.mit; # https://github.com/DaFuqs/PaginatedAdvancements/blob/6e8656733ad9cd5156573f4f9738cb0433f886c2/LICENSE
		};
	};
	#paladins-and-priests
	#patchouli
	#paxi
	#pehkui
	#philips-ruins
	#phosphor
	#physicsmod
	#pick-up-notifier
	#ping-wheel
	#plasmo-voice #TODO plugin?
	#playeranimator
	#playerhealthindicators
	#polymorph
	#presence-footsteps
	#prism-lib
	#projectile-damage-attribute
	#promenade
	#puzzle
	#puzzles-lib
	#raised
	#reacharound
	#rebind-narrator
	#rechiseled
	reeses-sodium-options.latestStable = fetchMetadataFromModrinth {
		modSlug = "reeses-sodium-options";
		versionID = "fkLiGoHs";
		packwizDeps = [ final.sodium.latestStable ];
		sha256 = "975478c93b1218131fd48695cd7efb9bbe0d69125409940b015a7058e42fb6be";
		meta = {
			description = "Adds GUI for configuring Sodium";
			license = licenses.mit; # https://github.com/FlashyReese/reeses-sodium-options/blob/mc1.20.4-1.7.1/LICENSE.txt
		};
	};
	#regions-unexplored
	#rei
	#replaymod
	#repurposed-structures-fabric
	#resourceful-config
	resourceful-lib.latestStable = fetchMetadataFromModrinth {
		modSlug = "resourceful-lib";
		versionID = "oRyi7tka";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "9f34262a83f0a15a09ca826a066f11520d9c79371c44716a49d54a524326c7cf";
		meta = {
			license = licenses.mit; # https://github.com/Team-Resourceful/ResourcefulLib/blob/v2.4.6/LICENSE
			modID = "resourcefullib";
		};
	};
	#resourcify
	#rightclickharvest
	#roughly-enough-professions-rep
	#roughly-searchable
	rrls.latestStable = fetchMetadataFromModrinth {
		modSlug = "rrls";
		versionID = "EzsoDheL";
		packwizDeps = [ final.cloth-config.latestStable ];
		sha256 = "937bd200c74fe8c5391a4c85b9e901ed35332c00b3757a82df545c14770a585a";
		meta = {
			description = "Allows interacting with the UI while resource packs are reloading";
			license = licenses.osl3; # https://github.com/dima-dencep/rrls/blob/1.20.4-4.0.1/LICENSE
		};
	};
	#runes
	#screenshot-to-clipboard
	searchables.latestStable = fetchMetadataFromModrinth {
		modSlug = "searchables";
		versionID = "siSgbXcb";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "9826c6020d1498b3c80d825f6ad272234c201b3ace8fb862e2aeebd7bb45fb74";
		meta = {
			license = licenses.mit; # https://github.com/jaredlll08/searchables/blob/52dcd1bf6ec6c7185a4dcb5d579e06210b915d4f/LICENSE
		};
	};
	#servercore
	#shoulder-surfing-reloaded
	#show-me-your-skin
	#shulkerboxtooltip
	#simple-discord-rpc
	#simple-voice-chat #TODO plugin?
	#simply-swords
	#skeleton-horse-spawn
	#sleep-warp
	#slice-and-dice
	#small-ships
	#smartbrainlib
	#smarter-farmers-farmers-replant
	#smooth-swapping
	#smoothboot-fabric
	#snow-real-magic
	#snow-under-trees-remastered
	sodium.latestStable = fetchMetadataFromModrinth {
		modSlug = "sodium";
		versionID = "4GyXKCLd";
		sha256 = "5e73967368d47038c38dc52fa298b45603fbe6baf2608f171638ff767178ad9a";
		meta = {
			description = "Replaces the rendering engine with one more suited to modern GPUs";
			license = licenses.lgpl3Only; # https://github.com/CaffeineMC/sodium-fabric/blob/mc1.20.3-0.5.6/COPYING.LESSER
		};
	};
	#sodium-extra
	#sodium-shadowy-path-blocks
	#sound-physics-remastered
	#spark
	#spawn-animations #TODO datapack?
	#spell-engine
	#spell-power
	#starlight
	#starter-kit
	status-effect-bars.latestStable = fetchMetadataFromModrinth {
		modSlug = "status-effect-bars";
		versionID = "i7dHnAbG";
		packwizDeps = [ final.cloth-config.latestStable ];
		sha256 = "7f148daee130716f4c5160113e635a6d067c28944994f84d2471beba4d9c2842";
		meta = {
			description = "Adds a subtle duration meter to status effect indicators";
			license = licenses.lgpl3Only; # https://github.com/A5b84/status-effect-bars/blob/v1.0.4/COPYING.LESSER
		};
	};
	#stendhal
	#stoneworks
	#superflat-world-no-slimes
	#supermartijn642s-config-lib
	#supermartijn642s-core-lib
	#supplementaries
	#supplementaries-squared
	talkbubbles.latestStable = fetchMetadataFromModrinth {
		modSlug = "talkbubbles";
		versionID = "BvZZmYcB";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "5956607e8d1d115911e16b8f074b85340f974779c72a10b348a7211ba94d8fa9";
		meta = {
			description = "Shows speech bubbles over players' heads when they chat";
			license = licenses.mit; # https://github.com/Globox1997/TalkBubbles/blob/2e01cec123e2035af678d69ed9284942cbdce022/LICENSE
		};
	};
	#tectonic #TODO datapack?
	#terra #TODO plugin?
	#terrablender
	#terralith
	#terrestria
	#the-bumblezone-fabric
	#the-graveyard-fabric
	#things
	#threadtweak
	tips.latestStable = fetchMetadataFromModrinth {
		modSlug = "tips";
		versionID = "C9l1iDV1";
		packwizDeps = [ final.bookshelf-lib.latestStable final.fabric-api.latestStable ];
		sha256 = "c9a3549b85eb5beb3bc42a16971dc47f2f957b9ee35ef7cfef0a9c88cac27676";
		meta = {
			description = "Adds protips to the pause and loading screens";
			license = licenses.lgpl21Only; # https://github.com/Darkhax-Minecraft/Tips/blob/f2853ad5610c9abdf3f374f146019b9d83662cc6/LICENSE
			modID = "tipsmod";
		};
	};
	#title-fixer
	#toms-storage
	tool-stats.latestStable = fetchMetadataFromModrinth {
		modSlug = "tool-stats";
		versionID = "cxBa2rPg";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "4fab5210b3c0741e7d529aa34b0816b3736d2937dc86feb1e999a5209b144a9a";
		meta = {
			description = "Includes basic details in the tooltips of tools and armour which are missing from vanilla";
			license = licenses.lgpl21Only; # https://github.com/Darkhax-Minecraft/Tool-Stats/blob/68c2bef0a95fa5d8c6bbe13cb45e0460d03dd0be/LICENSE
			modID = "toolstats";
		};
	};
	#tooltipfix
	#totemcounter
	#towns-and-towers
	#trading-post
	#travelers-titles
	#travelersbackpack
	#tree-harvester
	#trinkets
	#twigs
	ukulib.latestStable = fetchMetadataFromModrinth {
		modSlug = "ukulib";
		versionID = "1pdmPwYS";
		sha256 = "86ccc5953073d5f6ee7689ad3fc849e4d8220df723d3abb43c06e09eb3b9c263";
		meta = {
			license = licenses.mpl20; # https://github.com/uku3lig/ukulib/blob/1.1.0%2B1.20.3/LICENSE
		};
	};
	#universal-graves
	#universal_ores
	#valkyrien-skies
	#vanilla-refresh #TODO datapack?
	#vein-mining
	#viafabric
	viafabricplus.latestStable = fetchMetadataFromModrinth {
		modSlug = "viafabricplus";
		versionID = "Y8rSEWgC";
		sha256 = "64dadb3f8fc8333c1ed229067bea0ae9a1e338c0b64a373de2a1b3319050979e";
		meta = {
			description = "Allows connecting to servers running older versions of Minecraft";
			license = licenses.gpl3Only; # https://github.com/ViaVersion/ViaFabricPlus/blob/v3.0.6/LICENSE
		};
	};
	#village-spawn-point
	#villager-names-serilum
	#villagersplus
	#visual-overhaul
	#visual-workbench
	visuality.latestStable = fetchMetadataFromModrinth {
		modSlug = "visuality";
		versionID = "OR2HyGHb";
		packwizDeps = [ final.cloth-config.latestStable final.fabric-api.latestStable ];
		sha256 = "5ff3f8eb488d24fc70830b805da7a33d8fa9ffa1b45d858bf60189060c56093c";
		meta = {
			description = "Adds unique particles to a bunch of blocks and mobs";
			license = licenses.mit; # https://github.com/PinkGoosik/visuality/blob/0.7.3%2B1.20.4/LICENSE
		};
	};
	#vivecraft
	#vmp-fabric
	#voidz
	#voxelmap-updated
	#wakes
	#wavey-capes
	#waystones
	#weaponmaster
	#when-dungeons-arise
	#wilder-wild
	#wizards
	#worldedit #TODO plugin?
	#wthit
	#wynntils
	xaeros-minimap.latestStable = fetchMetadataFromModrinth {
		modSlug = "xaeros-minimap";
		versionID = "LLE04weG";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "6fea2f9bac56a998ca70374c89e788a7d5d78d0b59311f4fadb09ec91af99212";
		meta = {
			description = "Adds a configurable minimap to the HUD (which shares data with Xaero's World Map)";
			license = licenses.unfree;
			modID = "xaerominimap";
		};
	};
	xaeros-world-map.latestStable = fetchMetadataFromModrinth {
		modSlug = "xaeros-world-map";
		versionID = "Kh1mGgGA";
		packwizDeps = [ final.fabric-api.latestStable ];
		sha256 = "c6374b029408c864318be26f5e6854e1fd4c70222f0dacc4a4d8c1eadc364369";
		meta = {
			description = "Adds a global map with 1-block resolution and waypoints, among other things";
			license = licenses.unfree;
			modID = "xaeroworldmap";
		};
	};
	yacl.latestStable = fetchMetadataFromModrinth {
		modSlug = "yacl";
		versionID = "StXMrAsz";
		sha256 = "0e64714c116db266b1c35b0fea980d0ad75ddee7639d13f89d5cedf6c7e336e7";
		meta = {
			license = licenses.lgpl3Only; # https://github.com/isXander/YetAnotherConfigLib/blob/3.3.2%2B1.20.4/LICENSE
			modID = "yet_another_config_lib_v3";
		};
	};
	#yigd
	yosbr.latestStable = fetchMetadataFromModrinth {
		modSlug = "yosbr";
		versionID = "KMOzdYko";
		sha256 = "6f6334f336a876088431df70e2bba6ac52ca2ddeb73a4880ce88d6954b09af00";
		meta = {
			description = "Makes default config overrides only apply on first install (or when adding mods), not when updating";
			license = licenses.lgpl3Only; # https://github.com/shedaniel/your-options-shall-be-respected/blob/6ede5aa418f23a6601a8a6de3ceeb145b266c082/LICENSE
		};
	};
	#yungs-api
	#yungs-better-desert-temples
	#yungs-better-dungeons
	#yungs-better-end-island
	#yungs-better-jungle-temples
	#yungs-better-mineshafts
	#yungs-better-nether-fortresses
	#yungs-better-ocean-monuments
	#yungs-better-strongholds
	#yungs-better-witch-huts
	#yungs-bridges
	#yungs-extras
	#zombie-horse-spawn
	zoomify.latestStable = fetchMetadataFromModrinth {
		modSlug = "zoomify";
		versionID = "JiEpJuon";
		packwizDeps = [ final.fabric-api.latestStable final.fabric-language-kotlin.latestStable final.yacl.latestStable ];
		sha256 = "7005e926a32cfe6e1191ea0da9f65635e28f9dec4d827044c43da622697ba9c2";
		meta = {
			description = "Makes spyglass zoom configurable and optionally usable without a spyglass";
			license = licenses.lgpl3Only; # https://github.com/isXander/Zoomify/blob/2.13.0/LICENSE
		};
	};
}
