final: prev: let
	allModsFunc = import ./top-of-modrinth.nix {
		inherit (prev.minecraftTools) fetchMetadataFromModrinth;
		inherit (final) lib;
	};
	inherit (prev) lib;
	# this looks equivalent to `allMods // byID` but it is in fact equivalent to `byID // allMods`
	byIDOverlay = _: lib.mapAttrs'
		(_: value: { inherit value; name = (lib.head (lib.attrValues value)).meta.modID; });
	allMods = lib.fix (lib.extends byIDOverlay allModsFunc);
in {
	lib = prev.lib // {
		licenses = let
			l = prev.lib.licenses;
		in l // {
			tr7zw = l.unfree; # MIT (Expat) + NC clause
			tschipcraft = l.unfree; # bespoke with contradictory terms
			twelveIterations = l.unfree; # all (or most) rights reserved
		};
	};
	minecraftMods = allMods // {
		# unlock:
		freecam.latestStable = prev.fetchurl {
			url = "https://github.com/MinecraftFreecam/Freecam/releases/download/v1.2.3/freecam-fabric-1.2.3+1.20.4.jar";
			hash = "sha256-/lD3cdgk6OjuZGGA/f42NczezoTLjTlek3r9i8Ezsb8=";
			meta = allMods.freecam.latestStable.meta // { downloadPage = "https://github.com/MinecraftFreecam/Freecam"; };
		};
		freecamModrinthEdition = allMods.freecam;

		# aliases:
		removeReloadingScreen = allMods.rrls;
	};
}
