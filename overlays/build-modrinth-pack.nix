final: prev: let
	inherit (prev) lib fetchpatch linkFarm runCommand writeShellScript writeText writeTextDir;
in {
	minecraftTools = {
		buildModrinthPack = { pname, src, sha256, passthru ? {}, meta ? {} }: let
			/** extracting and recompressing the archive is necessary for r13y, but as a bonus it should result in a smaller archive */
			dearchived = runCommand "${pname}-dearchived" {
				inherit meta;
				nativeBuildInputs = [ prev.cacert final.packwiz prev.unzip ];
				outputHash = sha256;
				outputHashAlgo = "sha256";
				outputHashMode = "recursive";
			} ''
				cp -Lr '${src}' source; cd source; chmod +w index.toml pack.toml
				mkdir -p "$TMPDIR/cache"
				packwiz modrinth export --output "$TMPDIR/result.zip" --cache "$TMPDIR/cache"
				mkdir -p "$out"
				unzip -q "$TMPDIR/result.zip" -d "$out"
			'';
		in runCommand "${pname}" {
			inherit meta src;
			nativeBuildInputs = [ prev.zip ];
			passthru = { inherit dearchived; } // passthru;
		} ''cd '${dearchived}'; zip -r9 - * >"$out"''; # zip can't open "$out" for some reason, need to redirect # also this derivation isn't reproducible (wrong zip tool?)
		/**
		 * `packwizDeps`: isn't concerned with runtime, only what packwiz thinks is needed; as such,
		 * transitive deps are not used and may need to be added explicitly, and the Fabric API is treated as a monolith
		 */
		fetchMetadataFromModrinth = { modSlug, versionID, sha256, side ? "", packwizDeps ? [], meta ? {} }: let
			filename = modSlug + ".pw.toml";
			addDepsScript = if lib.length packwizDeps == 0 then "" else writeShellScript "add-deps"
				(lib.concatStrings (builtins.map (drv: "$1 '${drv}' '${drv.meta.modID}.pw.toml'\n") packwizDeps));
		in (runCommand filename {
			inherit addDepsScript modSlug side versionID;
			nativeBuildInputs = [ prev.cacert final.packwiz ];
			outputHash = sha256;
			outputHashAlgo = "sha256";
		} ''
			mkdir dummy; cd dummy
			packwiz init -y --mc-version 1.14 --modloader fabric --fabric-latest >/dev/null
			if [ "$addDepsScript" ]; then mkdir mods; (cd mods; "$addDepsScript" 'cp -vT'); packwiz refresh; fi
			packwiz modrinth add --version-id "$versionID"
			if [ ! -e "mods/$modSlug.pw.toml" ]; then ls mods; fi
			if [ "$side" ]; then sed '/side =/s/".*"/"'"$side"'"/' -i "mods/$modSlug.pw.toml"; fi
			grep -F 'side ' "mods/$modSlug.pw.toml"
			sha256sum "mods/$modSlug.pw.toml"
			mv "mods/$modSlug.pw.toml" "$out"
		'') // {
			inherit filename;
			isPackwizMetadata = true;
			meta = {
				downloadPage = "https://modrinth.com/mod/${modSlug}";
				modID = modSlug;
			} // meta;
		};
		joinPackwizModEntries = mods: final.minecraftTools.linkFarmToModsSubdir "mods" (drv: drv.filename) mods;
		linkFarmToModsSubdir = name: getFilename: entries: (linkFarm name (lib.listToAttrs (builtins.map
			(drv: { name = getFilename drv; value = drv; })
			entries))
		).overrideAttrs (oldAttrs: {
			/** HACK depends on implementation remaining as https://github.com/NixOS/nixpkgs/blob/23.11/pkgs/build-support/trivial-builders/default.nix#L582 */
			buildCommand = ''out="$out/mods"
			'' + oldAttrs.buildCommand;
		});
		writeOptionsTxt = let
			order = [
				"version"
				"doubleTapSprint"
				"autoJump"
				"operatorItemsTab"
				"autoSuggestions"
				"chatColors"
				"chatLinks"
				"chatLinksPrompt"
				"enableVsync"
				"entityShadows"
				"forceUnicodeFont"
				"discrete_mouse_scroll"
				"invertYMouse"
				"realmsNotifications"
				"reducedDebugInfo"
				"showSubtitles"
				"directionalAudio"
				"touchscreen"
				"fullscreen"
				"bobView"
				"toggleCrouch"
				"toggleSprint"
				"darkMojangStudiosBackground"
				"hideLightningFlashes"
				"hideSplashTexts"
				"mouseSensitivity"
				"fov"
				"screenEffectScale"
				"fovEffectScale"
				"darknessEffectScale"
				"glintSpeed"
				"glintStrength"
				"damageTiltStrength"
				"highContrast"
				"narratorHotkey"
				"gamma"
				"renderDistance"
				"simulationDistance"
				"entityDistanceScaling"
				"guiScale"
				"particles"
				"maxFps"
				"graphicsMode"
				"ao"
				"prioritizeChunkUpdates"
				"biomeBlendRadius"
				"renderClouds"
				"resourcePacks"
				"incompatibleResourcePacks"
				"lastServer"
				"lang"
				"soundDevice"
				"chatVisibility"
				"chatOpacity"
				"chatLineSpacing"
				"textBackgroundOpacity"
				"backgroundForChatOnly"
				"hideServerAddress"
				"advancedItemTooltips"
				"pauseOnLostFocus"
				"overrideWidth"
				"overrideHeight"
				"chatHeightFocused"
				"chatDelay"
				"chatHeightUnfocused"
				"chatScale"
				"chatWidth"
				"notificationDisplayTime"
				"mipmapLevels"
				"useNativeTransport"
				"mainHand"
				"attackIndicator"
				"narrator"
				"tutorialStep"
				"mouseWheelSensitivity"
				"rawMouseInput"
				"glDebugVerbosity"
				"skipMultiplayerWarning"
				"skipRealms32bitWarning"
				"hideMatchedNames"
				"joinedFirstServer"
				"hideBundleTutorial"
				"syncChunkWrites"
				"showAutosaveIndicator"
				"allowServerListing"
				"onlyShowSecureChat"
				"panoramaScrollSpeed"
				"debugifyTelemetry"
				"telemetryOptInExtra"
				"onboardAccessibility"
				"key_key.attack"
				"key_key.use"
				"key_key.forward"
				"key_key.left"
				"key_key.back"
				"key_key.right"
				"key_key.jump"
				"key_key.sneak"
				"key_key.sprint"
				"key_key.drop"
				"key_key.inventory"
				"key_key.chat"
				"key_key.playerlist"
				"key_key.pickItem"
				"key_key.command"
				"key_key.socialInteractions"
				"key_key.screenshot"
				"key_key.togglePerspective"
				"key_key.smoothCamera"
				"key_key.fullscreen"
				"key_key.spectatorOutlines"
				"key_key.swapOffhand"
				"key_key.saveToolbarActivator"
				"key_key.loadToolbarActivator"
				"key_key.advancements"
				"key_key.hotbar.1"
				"key_key.hotbar.2"
				"key_key.hotbar.3"
				"key_key.hotbar.4"
				"key_key.hotbar.5"
				"key_key.hotbar.6"
				"key_key.hotbar.7"
				"key_key.hotbar.8"
				"key_key.hotbar.9"
				"modKeybinds"
				"soundCategory_master"
				"soundCategory_music"
				"soundCategory_record"
				"soundCategory_weather"
				"soundCategory_block"
				"soundCategory_hostile"
				"soundCategory_neutral"
				"soundCategory_player"
				"soundCategory_ambient"
				"soundCategory_voice"
				"modelPart_cape"
				"modelPart_jacket"
				"modelPart_left_sleeve"
				"modelPart_right_sleeve"
				"modelPart_left_pants_leg"
				"modelPart_right_pants_leg"
				"modelPart_hat"
			];
			addQuotes = s: "\"${s}\"";
			flattenModKeybinds = bindsAttrset: lib.concatLists
				(lib.mapAttrsToList
					(_: binds: builtins.map (s: s + "\n") binds)
					bindsAttrset);
			formatResourcePacksList = list: "[${lib.concatStringsSep "," (builtins.map addQuotes list)}]";
			primitiveFormatters = {
				bool = lib.boolToString;
				float = f: let
					s = builtins.toString f; # what's up with `lib.strings.floatToString`?
					n = lib.strings.commonSuffixLength "00000" s;
				in if n > 0 then lib.substring 0 (lib.stringLength s - n) s else s;
				#int = builtins.toString;
				string = lib.id;
			};
			formatAsLinesWith = options: name: let
				value = options.${name};
				formatter = if lib.elem name [ "resourcePacks" "incompatibleResourcePacks" ]
					then formatResourcePacksList
					else if lib.elem name [ "renderClouds" "soundDevice" "mainHand" ]
						then addQuotes
						else primitiveFormatters.${builtins.typeOf value} or builtins.toString;
			in if name == "modKeybinds"
				then flattenModKeybinds value
				else lib.singleton "${name}:${formatter value}\n";
		in { options, filePath ? null }: lib.pipe order [
			(lib.filter (s: lib.hasAttr s options || s == "version"))
			(lib.concatMap (formatAsLinesWith ({ version = 3700; } // options)))
			lib.concatStrings
			(if filePath == null then writeText "options.txt" else writeTextDir filePath)
		];
	};
	packwiz = prev.packwiz.overrideAttrs (oldAttrs: {
		patches = (oldAttrs.patches or []) ++ [
			/** init fix https://github.com/packwiz/packwiz/pull/290 */
			(fetchpatch {
				url = "https://github.com/packwiz/packwiz/commit/5ac66aaec1132ff02a7f3ffe9710208672b7b1b6.patch";
				hash = "sha256-cJ8h1Ku8M0kYHO8MOnQMwbIrGHc+tDVW7GUWzphziWw=";
			})
			/** r13y fix https://github.com/packwiz/packwiz/pull/287 */
			(fetchpatch {
				url = "https://github.com/packwiz/packwiz/commit/cb46bced3b2aab1412ea633003c26b0e18152fb5.patch";
				hash = "sha256-PNTsdSF++kKBL6wjZpiEtdEaWTqZx2qGfaiuS6rQimw=";
			})
		];
		postPatch = (oldAttrs.postPatch or "") + ''
			# from https://github.com/packwiz/packwiz/issues/289
			# fixes 8937960d526793802dee43106ef779b9f1f7aafc
			sed 's/ && projectID == "" {/ \&\& versionID == "" {/' -i modrinth/install.go
		'';
	});
}
