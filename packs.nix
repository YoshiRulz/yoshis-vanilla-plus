{ lib, minecraftMods, minecraftTools, runCommand, runCommandLocal, symlinkJoin, writeTextDir }: let
	inherit (minecraftTools)
		buildModrinthPack fetchMetadataFromModrinth joinPackwizModEntries linkFarmToModsSubdir writeOptionsTxt;
	extraMods = import ./mods.nix { inherit lib fetchMetadataFromModrinth minecraftMods; };
	resultChecksumMap = { # keys: CA hash of source (combined with NIX_STORE_DIR); values: expected FOD hash
		"/nix/store/hm4p6x9sh21m2srrwy2s9wqcslzwndhq-yoshis-vanilla-plus-purer-src" = "sha256-8yJ5YH0XaPwaMb74+YrFkGoDjHm/zqx2QOEfWqI37OQ=";
		"/nix/store/cxhggpic8863y842yhbxp66i5dmv056s-yoshis-vanilla-plus-pure-src" = "sha256-hNJjpgtbHsf/jb+RkG1nHCdwl5gmr5CqlanRwLQSEFU=";
		"/nix/store/lx7qdpj0ndiqdwp5968jqjzi16145sb8-yoshis-vanilla-plus-impure-src" = "sha256-mr84zH+PQZS+wQtbiaietaeF85kBovjxOn+RxHfQjAk=";
		"/nix/store/v2xyjq1rsqq9l8yhbsva590z5rc0mbsd-yoshis-vanilla-plus-yoshi-src" = "sha256-q4xlh0K+j/bzPa+lDSgbb4KMfpplyyYOnJBWqOhcE0g=";
	};
	partialFlavours = lib.mapAttrs
		(flavour: { parent ? null, modsOrMetadata ? [], sourceOverlays ? [] }: let
			pname = "yoshis-vanilla-plus-${flavour}";
			srcOnDisk = ./src-${flavour};
			srcInStore0 = builtins.path {
				filter = (_: type: type != "symlink");
				name = "${pname}-src";
				path = srcOnDisk;
			};
			modsOrMetadata1 = if lib.isAttrs modsOrMetadata then lib.attrValues modsOrMetadata else modsOrMetadata;
			modsMeta = builtins.map (drv: drv.meta) modsOrMetadata1;
			sourceOverlays1 = let
				inherit (lib.partition (drv: drv.isPackwizMetadata or false) modsOrMetadata1) right wrong;
			in lib.optional (right != []) (joinPackwizModEntries right)
				++ lib.optional (wrong != []) (linkFarmToModsSubdir "prepackaged-mods" (drv: drv.name) wrong)
				++ sourceOverlays;
			srcInStore = if lib.length sourceOverlays1 == 0
				then srcInStore0
				else symlinkJoin {
					name = "${pname}-src";
					paths = [ srcInStore0 ] ++ sourceOverlays1;
				};
		in {
			inherit modsMeta parent pname srcInStore srcOnDisk;
			license = lib.unique (builtins.map (meta: meta.license) modsMeta);
			sourceOverlays = sourceOverlays1;
			srcHash = builtins.unsafeDiscardStringContext srcInStore;
		})
		{
			purer = {
				modsOrMetadata = lib.attrValues (lib.mapAttrs (_: value: value.latestStable) {
					inherit (minecraftMods)
						adaptive-tooltips appleskin architectury-api better-enchanted-books better-mount-hud boat-item-view
						bobby bookshelf-lib chat-heads chatpatches cherished-worlds chunks-fade-in cloth-config
						completeconfig creativecore debugify enchantment-descriptions enhancedblockentities fabric-api
						fabric-language-kotlin forge-config-api-port freecam gamma-utils immediatelyfast indium krypton
						letmedespawn lithium malilib memoryleakfix modelfix modernfix moreculling mouse-wheelie noisium
						nvidium sodium status-effect-bars tips tool-stats viafabricplus yacl yosbr zoomify;
				} // {
					inherit (extraMods)
						armor-chroma-for-fabric bettercommandblockui breakfree cake-chomps chatanimation clientsidenoteblocks
						draggable-lists effect-tooltips fast-ip-ping fastopenlinksandfolders friendly-fire harvest
						head-in-the-clouds held-item-info here-be-no-dragons itemphysic-lite let-sleeping-dogs-lie
						multiplayer-server-pause panorama-screens pinglist realisticsleep see-through-waterlava simplefog
						stack-to-nearby-chests vectorientation "x+-autofish";
				});
				sourceOverlays = [
					(writeOptionsTxt {
						options = import ./files/options-purer.nix;
						filePath = "config/yosbr/options.txt";
					})
					(let
						names = { base = "Yoshi's Protips (Base Set)"; };
						protipsBase = builtins.path {
							name = "yoshis-protips-base";
							path = /home/yoshi/Documents/mc_temp/resource_packs/${names.base};
							#sha256 = ""; #TODO
						};
					in runCommandLocal "resourcepacks" {} ''
						out="$out/config/yosbr/resourcepacks"
						mkdir -p "$out"
						ln -s '${protipsBase}' "$out/${names.base}"
					'')
					# for TOML files: `(pkgs.formats.toml {}).generate "pack.toml" tomlObj`
					#TODO eventually generate everything with Nix; the index.toml is just a lockfile with plain sha256 checksums, meaning the individual mod files can be FODs, and the pack.toml can be stripped down since it's regenerated during the export
				];
			};
			pure = {
				parent = "purer";
				modsOrMetadata = lib.attrValues (lib.mapAttrs (_: value: value.latestStable) {
					inherit (minecraftMods)
						alternate-current antighost auth-me badoptimizations cleardespawn clumps controlling craftpresence
						dynamic-fps e4mc emi emitrades enhanced-attack-indicator fallingleaves fastquit ferrite-core
						highlight lambdynamiclights language-reload midnightlib modmenu no-chat-reports
						no-resource-pack-warnings notenoughcrashes owo-lib paginatedadvancements reeses-sodium-options
						resourceful-lib searchables talkbubbles ukulib visuality;
				} // {
					inherit (extraMods)
						ambient-environment async-locator blame-log chathighlighter command-macros emi-enchanting mcpings
						moddetectionpreventer new-infinity-fix no-night-vision-flickering norecipebook-fabric particle-rain
						petowner rebind-all-the-keys ridingmousefix serverpingerfixer smoothmenu-refabricated
						snowball-and-egg-knockback stairdoors super-secret-settings tiny-item-animations trade-uses
						ukus-armor-hud;
				});
				sourceOverlays = [
					(writeTextDir "config/fabric_loader_dependencies.json" (builtins.toJSON {
						version = 1;
						overrides = {
							infinityfix = { "-depends" = { "minecraft" = ""; }; };
						};
					}))
				];
			};
			impure = {
				parent = "pure";
				modsOrMetadata = lib.attrValues (lib.mapAttrs (_: value: value.latestStable) {
					inherit (minecraftMods)
						"3dskinlayers" balm better-end-sky client-tweaks entityculling equipment-compare exordium iceberg
						item-highlighter jade kleeslabs make_bubbles_pop not-enough-animations removeReloadingScreen
						xaeros-minimap xaeros-world-map;
				} // {
					inherit (extraMods)
						better-than-llamas cat_jam companion cooperative-advancements visiblebarriers xaero-zoomout;
				});
			};
			yoshi = {
				parent = "impure";
				sourceOverlays = [
					(writeOptionsTxt {
						options = import ./files/options-yoshi.nix;
						filePath = "config/yosbr/options.txt";
					})
				];
			};
		};
	flavours = lib.mapAttrs
		(_: part: part // lib.optionalAttrs (part.parent != null) {
			srcInStore = symlinkJoin {
				name = "${part.pname}-src-withparent";
				paths = [ flavours.${part.parent}.srcInStore part.srcInStore ];
			};
		})
		partialFlavours;
in lib.mapAttrs
	(flavour: { license, pname, srcHash, srcInStore, srcOnDisk, ... }: buildModrinthPack {
		inherit pname;
		src = srcInStore;
		sha256 = resultChecksumMap.${srcHash} or (lib.trace "need to add ${srcHash} to checksum map" "");
		passthru = { inherit srcOnDisk; };
		meta = { inherit license; };
	})
	flavours
