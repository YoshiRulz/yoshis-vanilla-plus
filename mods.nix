{ lib, fetchMetadataFromModrinth, minecraftMods }: let
	inherit (lib) licenses;
	m = minecraftMods;
in lib.mapAttrs (_: fetchMetadataFromModrinth) {
	ambient-environment = {
		modSlug = "ambient-environment";
		versionID = "oLpdcxoy";
		sha256 = "892990647a9cbbd5cba8273cbe49681b33e7770143863879c972bc2f454b406d";
		meta = {
			description = "Introduces noise to the foliage and water biome colour gradients";
			license = licenses.mit; # https://github.com/jaredlll08/Ambient-Environment/blob/b3512d93d47f24daa26bd60ca9ce74b07aee191d/fabric/src/main/resources/fabric.mod.json#L14
			modID = "ambientenvironment";
		};
	};
	armor-chroma-for-fabric = {
		modSlug = "armor-chroma-for-fabric";
		versionID = "iat3A4y4";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "a8f06b1711c4a7ad809d5d03c78df38bf68c68ffa9876f43cc9ba41db0de4ba2";
		meta = {
			description = "Improves the armour points HUD";
			license = licenses.gpl3Only; # https://github.com/A5b84/armor-chroma-fabric/blob/v1.2.6/LICENSE
			modID = "armorchroma";
		};
	};
	async-locator = {
		modSlug = "async-locator";
		versionID = "xXhZI4u3";
		side = "both";
		sha256 = "b0c84806bc32028e006144a81865ff5180614bb8146ffc265a905bb2c6c3e1bb";
		meta = {
			description = "Optimises the structure pathfinding routine used for dolphins and `/locate`";
			license = licenses.mit; # https://github.com/thebrightspark/AsyncLocator/blob/1.20-1.3.0/LICENSE
			modID = "asynclocator";
		};
	};
	better-than-llamas = {
		modSlug = "better-than-llamas";
		versionID = "96ivnRJF";
		packwizDeps = [ m.completeconfig.latestStable m.fabric-api.latestStable ];
		sha256 = "70584ea1232127da5bd0aa6fdf067d7307d1f238b5753593274b45d8c47e3960";
		meta = {
			description = "Adds fezzes and monocles to llama models";
			license = licenses.lgpl3Only; # https://github.com/iChun/BetterThanLlamas/blob/cb4bf59a5be674074ec8657ad6b9277454a6ad54/COPYING.LESSER
			modID = "betterthanllamas";
		};
	};
	bettercommandblockui = {
		modSlug = "bettercommandblockui";
		versionID = "MJSvOZHK";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "d030f3a7ed078e309b74fd7fd5ed5e5ba8791f0e4975934f8ab0dafb6efb91b9";
		meta = {
			description = "Replaces the command block's interface";
			license = licenses.cc0; # https://github.com/Tectato/BetterCommandBlockUI/blob/22ea05f6c106e49672367e847496c87cee798d77/LICENSE
		};
	};
	blame-log = {
		modSlug = "blame-log";
		versionID = "OTln9Rba";
		sha256 = "003f64df0a3cfd815b9172c126b9731f6b1c2154e0ede1499453f24f64c80cbd";
		meta = {
			description = "Improves other mods' logging to help identify which mod is misbehaving";
			license = licenses.mit; # https://github.com/melontini/blame-log/blob/0.4.0-1.18/LICENSE
		};
	};
	breakfree = {
		modSlug = "breakfree";
		versionID = "n2FrxATJ";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "cd2d1e6dd9babc2d7371760004701ee53651089f01894cf9f641cad5c8ab6cab";
		meta = {
			description = "Makes block break progress not reset if you switch tools, reverting a change from 1.4.4 (yes that's over a decade ago and I'm still salty)";
			license = licenses.unlicense; # https://github.com/JustAPotota/fabric-breakfree/blob/7e24b3b9cea517ea51e8f4822ba7c2c5222fe61a/LICENSE
		};
	};
	cake-chomps = {
		modSlug = "cake-chomps";
		versionID = "tgh1HIaf";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "cf3325da2b17a9a9608f87147a9cbe0b8bfbacde953df12adeca88e583c5ed8c";
		meta = {
			description = "Adds audio and additional visual feedback to eating cakes";
			license = licenses.lgpl3Plus; # https://github.com/illusivesoulworks/cakechomps/blob/45cb6dbe6013a3369b0a48cd5eefc9a571a5121b/gradle.properties#L14
			modID = "cakechomps";
		};
	};
	cat_jam = {
		modSlug = "cat_jam";
		versionID = "D5INFJwK";
		sha256 = "ea754cf20adb758fd52e23a53d6ada63a0f54351e8794f61ea935856982eccb9";
		meta = {
			description = "Adds dancing animations to cats like parrots have, so you can catJAM";
			license = licenses.cc-by-nc-sa-40; # https://github.com/shmove/cat_jam/blob/dbafa947a2d71653377c4d0637f38d09f91663a0/LICENSE.md
		};
	};
	chatanimation = {
		modSlug = "chatanimation";
		versionID = "hn2TlcOt";
		sha256 = "c66ca4fd2b94b0ed0cb3002040839aefcb71a8fd08f2dc3337e61f2e4ce28cf9";
		meta = {
			description = "Adds an opening animation to chat and animates incoming messages";
			license = licenses.cc0; # https://github.com/Ezzenix/ChatAnimation/blob/6ed86286471101d46980a350bc4caad5a76d5118/LICENSE
		};
	};
	chathighlighter = {
		modSlug = "chathighlighter";
		versionID = "dLEgJiba";
		packwizDeps = [ m.ukulib.latestStable ];
		sha256 = "ff925176d9787b0626128002fbdb58c5fc8b2a1260802e0c43a61f075a3f8797";
		meta = {
			description = "Allows highlighting mentions of your username (nickping), or any other keyword, in chat";
			license = licenses.mit; # https://github.com/uku3lig/chathighlighter/blob/6597010a885cf538935858c2fd9fadcaa8b71e42/LICENSE
		};
	};
	clientsidenoteblocks = {
		modSlug = "clientsidenoteblocks";
		versionID = "5Gaaacbx";
		sha256 = "c5bc14470385ce4b357d2490fe9da6b936e17035db4c0c006cab333615555dad";
		meta = {
			description = "Plays noteblock samples when the client sees them activate, not when the sound event is received from the server";
			license = licenses.lgpl3Only; # https://github.com/varun7654/ClientSideNoteblocks/blob/d3cbaa203e558c01c3d8584a53138a3f52c1c04a/LICENSE.md
		};
	};
	command-macros = {
		modSlug = "command-macros";
		versionID = "FeUSjUHZ";
		sha256 = "7503f57025e0a4fe1728fb56ef7406265cbd0d752bf27e10aad791379b0d3d72";
		meta = {
			description = "Allows sending arbitrary chat messages and commands with keyboard shortcuts";
			license = licenses.mit; # https://github.com/kyrptonaught/CMDKeybinds/blob/3d4b0ec88426ad27ee479041f647ea6f4842f0aa/LICENSE
			modID = "cmdkeybind";
		};
	};
	companion = {
		modSlug = "companion";
		versionID = "gqjOYbM8";
		side = "both";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "97cca2014971c73073f0ce99fcd0f27ad56620949c0c009b77aabcb5a6ea7aa9";
		meta = {
			description = "Improves pet following and combat mechanics";
			license = licenses.unfree; # no license: https://github.com/Snownee/Companion/tree/ea2cf0d517c2c2b04db5e0dd2b4ace441c0e5d2c
		};
	};
	cooperative-advancements = {
		modSlug = "cooperative-advancements";
		versionID = "EFEVC0Tw";
		side = "both";
		sha256 = "c150b11d4b6aa49e6b9cc97e849d49dc1e4e1cdc483ca625e3cdfc4473651d1e";
		meta = {
			description = "Synchronises advancement progress between players on the same team";
			license = licenses.cc-by-nc-nd-40; # https://github.com/AHilyard/CooperativeAdvancements/blob/768d5678a10a9afbbef0c3670fd96a2acbd2177e/LICENSE
			modID = "cooperativeadvancements";
		};
	};
	draggable-lists = {
		modSlug = "draggable-lists";
		versionID = "6YV2R6RF";
		sha256 = "aa58e80b2e7736180e29833193fc0370fb50912fd21a08d476a1898c1f7672f6";
		meta = {
			description = "Makes the resource pack and server lists drag-and-drop";
			license = licenses.lgpl3Only; # https://github.com/mrmelon54/draggable_lists/blob/v1.0.5%2B1.20.4/LICENSE.md
			modID = "draggable_lists";
		};
	};
	effect-tooltips = {
		modSlug = "effect-tooltips";
		versionID = "hxWwFVjO";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "440155f692a17f43d5a01c0e0b1e4591e3af9c4b5d33ec4d03110bdd2770a655";
		meta = {
			description = "Adds tooltips to the potion effect indicators in the inventory";
			license = licenses.lgpl21Only; # https://github.com/Darkhax-Minecraft/Effect-Tooltips/blob/08bb0499d2ed92703120fe9d90c50aadf08ac73d/LICENSE
			modID = "effecttooltips";
		};
	};
	emi-enchanting = {
		modSlug = "emi-enchanting";
		versionID = "EQYdSyh8";
		packwizDeps = [ m.emi.latestStable ];
		sha256 = "f88b04c5b537da9f1d032e3c9dff56648d9c19b7658af065f11d618f5c1c3fdd";
		meta = {
			description = "Enchanting plugin for EMI";
			license = licenses.mit; # https://github.com/fzzyhmstrs/EMI_enchanting/blob/a7bfd1a64985cd0be04339260fb5196b776297c4/src/main/resources/fabric.mod.json#L16
			modID = "emi_enchanting";
		};
	};
	fast-ip-ping = {
		modSlug = "fast-ip-ping";
		versionID = "U5CYxkEG";
		sha256 = "dc16da8c8360b4f7d869874d18188da7389d6beb143d7e077ef9ba70e7fd65d3";
		meta = {
			description = "Removes a blocking networking call when connecting to LAN servers which only adds delay";
			license = licenses.lgpl3Only; # https://github.com/Fallen-Breath/fast-ip-ping/blob/v1.0.1/LICENSE
			modID = "fastipping";
		};
	};
	fastopenlinksandfolders = {
		modSlug = "fastopenlinksandfolders";
		versionID = "DxXiUerV";
		sha256 = "ff54403726ef6349d2b0d9fc9588e5923aff79c0756820f3d4aa2c4c9dd8e2d8";
		meta = {
			description = "Fixes huge delays on `xdg-open`-like features (`Runtime.exec`), such as \"Open Resource Pack Folder...\"";
			license = licenses.lgpl3Only; # https://github.com/altrisi/FastOpenLinksAndFolders/blob/1.1.1/LICENSE
		};
	};
	friendly-fire = {
		modSlug = "friendly-fire";
		versionID = "yrRwKRe8";
		packwizDeps = [ m.bookshelf-lib.latestStable m.fabric-api.latestStable ];
		sha256 = "2acfd738fa65655c7eb677bf61e9a69711592bb01e3fadb8926a7d1699860a58";
		meta = {
			description = "Prevents you from accidentally attacking your pets";
			license = licenses.lgpl21Only; # https://github.com/Darkhax-Minecraft/Friendly-Fire/blob/8a9f8dd3b8a9c09d649d9142ba613c1d7059c39c/LICENSE
			modID = "friendlyfire";
		};
	};
	harvest = {
		modSlug = "harvest";
		versionID = "sYlEC6A2";
		sha256 = "5e3ae618bbae84cd594e64a56db95c455821ee657bf9558333837ec543938689";
		meta = {
			description = "Allows \"smart harvest\" of crops with right-click";
			license = licenses.lgpl3Plus; # https://github.com/n643064/Harvest-Fabric/blob/1.3/LICENSE
		};
	};
	head-in-the-clouds = {
		modSlug = "head-in-the-clouds";
		versionID = "M2AShHrv";
		packwizDeps = [ m.cloth-config.latestStable ];
		sha256 = "be062570338c15b4436a19013e04119f1d54094ea5d264f7e5f34b584fce4010";
		meta = {
			description = "Makes it not rain above where the clouds are rendered";
			license = licenses.gpl3Only; # https://github.com/DaFuqs/HeadInTheClouds/blob/3ba048b290d59ac2c1eae9559ef7cd61191333cd/LICENSE
			modID = "head_in_the_clouds";
		};
	};
	held-item-info = {
		modSlug = "held-item-info";
		versionID = "Rir70mD8";
		sha256 = "c24e05bc69b5e571da13b6f854c89ea093deab2b74b1bd3e8f17113a2f01c07e";
		meta = {
			description = "Adds important details to the held item name above the hotbar";
			license = licenses.lgpl3Only; # https://github.com/A5b84/held-item-info/blob/v1.5.0/COPYING.LESSER
		};
	};
	here-be-no-dragons = {
		modSlug = "here-be-no-dragons";
		versionID = "Fi3C0IO5";
		sha256 = "b521cb1efedc7ce0443e6df6ea00c10beb0000c769f36812840ba83abc7aa520";
		meta = {
			description = "Removes the confirmation screen when loading worlds with custom worldgen datapacks";
			license = licenses.lgpl3Only; # https://github.com/Parzivail-Modding-Team/HereBeNoDragons/blob/c92d8e110347bcee985e648f7ac2be5e3335b8e3/LICENSE
		};
	};
	itemphysic-lite = {
		modSlug = "itemphysic-lite";
		versionID = "uD3L1KRv";
		packwizDeps = [ m.creativecore.latestStable m.fabric-api.latestStable ];
		sha256 = "b1b968af070dd32cd89723f6b3d1b3a56985c44a5bd3b4b4e3d243912e6ca1ec";
		meta = {
			description = "Changes dropped items to sit naturally on the ground rather than float and spin";
			license = licenses.lgpl3Only; # https://github.com/CreativeMD/ItemPhysicLite/blob/81442798c8b072335a25eeb67049478b8095e531/LICENSE
			modID = "itemphysiclite";
		};
	};
	let-sleeping-dogs-lie = {
		modSlug = "let-sleeping-dogs-lie";
		versionID = "HlCLgvr2";
		packwizDeps = [ m.completeconfig.latestStable m.fabric-api.latestStable ];
		sha256 = "860b1f9f0d1abcc1829fb8765bb32451f2fc1e51c01678400d08bab70089a49e";
		meta = {
			description = "Gives sitting wolves a lying down animation, used when no-one's around";
			license = licenses.lgpl3Only; # https://github.com/iChun/LetSleepingDogsLie/blob/e6edcdc43db09f8223ddfcf5024600f7e90f660c/COPYING.LESSER
			modID = "dogslie";
		};
	};
	mcpings = {
		modSlug = "mcpings";
		versionID = "IMkhNTeR";
		packwizDeps = [ m.owo-lib.latestStable ];
		sha256 = "f7c722826742da5fce54d5bb3a8d72fcda264592f77cabc624786d8b28c04084";
		meta = {
			description = "Adds a virtual laser pointer function for indicating a location or entity to other players";
			license = licenses.mit; # https://github.com/Dyvinia/MCPings/blob/02adf3bb1f4b3c4a87700f55ab96a0dd056d3575/LICENSE
		};
	};
	moddetectionpreventer = {
		modSlug = "moddetectionpreventer";
		versionID = "r2Smj2SL";
		sha256 = "d5b31676ddcb673733e322e0e3b15894924ab6136a28b3573462b2307b6fd56b";
		meta = {
			description = "Patches a security hole which would allow the server you're on to detect whether you have a certain mod installed";
			license = licenses.mit; # https://github.com/JustAlittleWolf/ModDetectionPreventer/blob/1.1.0/LICENSE
		};
	};
	multiplayer-server-pause = {
		modSlug = "multiplayer-server-pause";
		versionID = "kkWnJtUT";
		packwizDeps = [ m.completeconfig.latestStable m.fabric-api.latestStable ];
		sha256 = "379447cc0778c0ac7bbffd0510ad89418f973de7e27ba6801583e25cc64b7268";
		meta = {
			description = "Lets the server pause ticking if all connected players have the mod and are paused";
			license = licenses.lgpl3Only; # https://github.com/iChun/MultiplayerServerPause/blob/1b5822cce5136ec4d81e510376332366f4f5981f/COPYING.LESSER
			modID = "serverpause";
		};
	};
	new-infinity-fix = {
		modSlug = "new-infinity-fix";
		versionID = "o9cphBKH";
		side = "both";
		sha256 = "2de1571e8117f43e16f888c6ef3a22451b1cf9170779bf458b57faabfbac4128";
		meta = {
			description = "Removes the need to carry around a useless arrow for an Infinity bow";
			license = licenses.mit; # https://github.com/MrLoLf/InfinityFix/blob/dcd36d2e682612165abb44239c73ad3740915869/LICENSE
			modID = "infinityfix";
		};
	};
	no-night-vision-flickering = {
		modSlug = "no-night-vision-flickering";
		versionID = "Ktqk9spu";
		sha256 = "4f818e0cf25d689e726ac5b6ff3b95c65db03046beb447981a9d049ec388d30b";
		meta = {
			description = "Replaces the Night Vision enchantment's flickering with a fade out";
			license = licenses.mit; # https://github.com/semper-idem/nonvflickering/blob/15c95ce6574029a7f15576d2f2a0599133299557/LICENSE
			modID = "nnvf";
		};
	};
	norecipebook-fabric = {
		modSlug = "norecipebook-fabric";
		versionID = "en6zyLhW";
		sha256 = "faf4c5b4fb912e282168478c896fe23d6cd913db493c81a6ec9224f57eff4bb1";
		meta = {
			description = "Removes the vanilla recipe book (since EMI doesn't)";
			license = licenses.lgpl3Only; # https://github.com/Grayray75/NoRecipeBook/blob/v3.1/LICENSE.txt
			modID = "norecipebook";
		};
	};
	panorama-screens = {
		modSlug = "panorama-screens";
		versionID = "1sv66aTk";
		sha256 = "198a068a8b44a00375ce5d3c21504a93a2c2ab42ce9f9b54f010514a0981adc9";
		meta = {
			description = "Uses the panorama from the main menu instead of a dirt background for most menus";
			license = licenses.gpl3Plus; # https://github.com/juancarloscp52/panorama-screens/blob/4ae843f3214beffb830fd6114e8bd50ba83eee3a/LICENSE
			modID = "panorama_screens";
		};
	};
	particle-rain = {
		modSlug = "particle-rain";
		versionID = "soqqh6x4";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "a874d77f08289eb1b597c0b63eb1abe85263ff69648369057a99382d9c608da1";
		meta = {
			description = "Replaces the ugly animated sprites for rain and snow with particles";
			license = licenses.mit; # https://github.com/PigCart/particle-rain/blob/v2.0.8/LICENSE
			modID = "particlerain";
		};
	};
	petowner = {
		modSlug = "petowner";
		versionID = "FgfJC2uc";
		packwizDeps = [ m.midnightlib.latestStable ];
		sha256 = "92e2642359ad0abb0e3407eaa6577e374754b6fd2e5f7f76e24313e3d88a191c";
		meta = {
			description = "Shows who owns a pet when you look at one";
			license = licenses.mit; # https://github.com/PotatoPresident/PetOwner/blob/ed57b9de2ae863e26689d7a0ea039a51c0c3d65b/LICENSE
		};
	};
	pinglist = {
		modSlug = "pinglist";
		versionID = "EMhMAxZU";
		sha256 = "6ff50dfa8ba9b72cfde99e6796eeccc35b750574b6eebbc94a8673c052e26cbf";
		meta = {
			description = "Replaces the \"reception bars\" in the player list with a numeric readout";
			license = licenses.mpl20; # https://gitlab.com/Raspberry-Jam/pinglist/-/blob/1.4/LICENSE
		};
	};
	realisticsleep = {
		modSlug = "realisticsleep";
		versionID = "vCoqEfJD";
		side = "both";
		packwizDeps = [ m.cloth-config.latestStable m.fabric-api.latestStable ];
		sha256 = "5d781de9776169913698e45fb36ac97727aea8b74d55d3b59ce8594bbe4c08d4";
		meta = {
			description = "Makes sleeping fast-forward time instead of skipping time";
			license = licenses.lgpl21Only; # https://github.com/Steveplays28/realisticsleep/blob/v1.10.2%2Bmc1.20.3-1.20.4/LICENSE
		};
	};
	rebind-all-the-keys = {
		modSlug = "rebind-all-the-keys";
		versionID = "pk9mFiKX";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "8b73665ae1accfbd68fdee4adff2a3eb42b3158bfc2193b01e75657ae24d5c75";
		meta = {
			description = "Allows all of the base game's hidden keybinds to be changed";
			license = licenses.mit; # https://github.com/Minenash/RebindAllTheKeys/blob/v1.4.0%2B1.20.2/LICENSE
			modID = "rebind_all_the_keys";
		};
	};
	ridingmousefix = {
		modSlug = "ridingmousefix";
		versionID = "IocX87m3";
		sha256 = "a1226a7f792f4b48a1f22ac75f41b5cf0ac23df37e0e272e102bc76d40fb1685";
		meta = {
			description = "Fixes camera movement when mounted";
			license = licenses.mit; # https://github.com/JustAlittleWolf/RidingMouseFix/blob/1.0.0/LICENSE
		};
	};
	see-through-waterlava = {
		modSlug = "see-through-waterlava";
		versionID = "rXxcLi4c";
		packwizDeps = [ m.cloth-config.latestStable ];
		sha256 = "f99bc9ba2f166d753205028d60b5bb99c4ac9c66c5a352dcca36606f71dd8feb";
		meta = {
			description = "Increases visibility in water and lava";
			license = licenses.gpl3Only; # https://github.com/spoorn/SeeThroughLava/blob/4d1bb515d113ff793ece468a16d29521d7ea372c/LICENSE
			modID = "seethroughlava";
		};
	};
	serverpingerfixer = {
		modSlug = "serverpingerfixer";
		versionID = "fHvfA2My";
		side = "both";
		sha256 = "099a626df5a23fca2445e691ac96946ca12bfd5d9bbe49e0c7055e1c2c5f6759";
		meta = {
			description = "Makes loading/refreshing MotDs for the server list faster";
			license = licenses.mit; # https://github.com/JustAlittleWolf/ServerPingerFixer/blob/1.0.4/LICENSE
		};
	};
	simplefog = {
		modSlug = "simplefog";
		versionID = "jDY7kR4t";
		packwizDeps = [ m.cloth-config.latestStable ];
		sha256 = "1ecb59adbb103f86496203ff3f44a80b18193512e74fd7a367ed06234702059f";
		meta = {
			description = "Increases visibility in the nether and fixes the discolouration of distant chunks";
			license = licenses.cc0; # https://github.com/Draradech/SimpleFogControl/blob/v1.4.0/LICENSE
		};
	};
	smoothmenu-refabricated = {
		modSlug = "smoothmenu-refabricated";
		versionID = "637Rl8Ch";
		sha256 = "3d148050146de53864628e6f2b20032ed362236bc84f3486df286f8c7bab5f3d";
		meta = {
			description = "Removes the framerate restriction on the main menu";
			license = licenses.mit; # https://github.com/shizotoaster/SmoothMenuRefabricated/blob/1.1.0/LICENSE
			modID = "smoothmenu";
		};
	};
	snowball-and-egg-knockback = {
		modSlug = "snowball-and-egg-knockback";
		versionID = "UuYZkFuD";
		side = "both";
		sha256 = "1cee6c3a10968b4c89f4887a24119591611388a19ec96e1d5ae2f6674526e7b8";
		meta = {
			description = "Allows using snowballs (and eggs) as a 0-damage attack against players";
			license = licenses.mit; # https://github.com/capitalistspz/SnowballKB/blob/1.2-1.20/LICENSE
			modID = "snowballkb";
		};
	};
	stack-to-nearby-chests = {
		modSlug = "stack-to-nearby-chests";
		versionID = "trUKwUKu";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "e34c37fe312e8b415d03fa1fe45da51d0b588d437e7b3a19bfb5d0e06535f8d8";
		meta = {
			description = "Adds a few simple storage management features";
			license = licenses.lgpl3Plus; # https://github.com/xiaocihua/stack-to-nearby-chests/blob/mc1.20.4-0.5.4/LICENSE
		};
	};
	stairdoors = {
		modSlug = "stairdoors";
		versionID = "vmsUK2mO";
		sha256 = "7f0eceb2ee3f5bd9d27238f2a86a6c8277b2c0bc7ae591c1d0611735df707129";
		meta = {
			description = "Allows placing doors on the half-block part of stairs";
			license = licenses.mit; # https://gitlab.com/Setadokalo/stairdoors-fabric/-/blob/28281c2940d6e6aa06bdd91203c28cff4b80b924/LICENSE
			modID = "stairdoors-fabric";
		};
	};
	super-secret-settings = {
		modSlug = "super-secret-settings";
		versionID = "Qm4hQiUn";
		sha256 = "6ac9608eaae701fda4a73ffed618eb69c8d89725aaaaf6af6553a4d23d031d9f";
		meta = {
			description = "Restores the experimental shaders from vanilla 1.7.2";
			license = licenses.mit; # https://github.com/fxys/Super-Secret-Settings/blob/7dc34fb75d085967b4dd8cad5b18448929d4afbd/LICENSE
			modID = "sss";
		};
	};
	tiny-item-animations = {
		modSlug = "tiny-item-animations";
		versionID = "TmolU2Dw";
		sha256 = "85637a09fe2f619c54117e29e9f562aa8e91d5942733df5f6ac19c966348d1cd";
		meta = {
			description = "Adds a subtle animation to picking up and placing items with the cursor";
			license = licenses.mit; # https://github.com/Trivaxy/Tiny-Item-Animations/blob/a137395e971974aae976672e7c07ed2d9c375e54/LICENSE
			modID = "tia";
		};
	};
	trade-uses = {
		modSlug = "trade-uses";
		versionID = "U7sV3xet";
		sha256 = "eb4073be81a5ae131338a3f8234b428b1cedb94c5525bb306e4dc4cd4f4d7e84";
		meta = {
			description = "Shows how many times each of a villager's trade offers can be used before they go \"out of stock\"";
			license = licenses.mit; # https://github.com/Khajiitos/TradeUses/blob/050988b88161c485d4d28669f0baf7789c385074/LICENSE
			modID = "tradeuses";
		};
	};
	ukus-armor-hud = {
		modSlug = "ukus-armor-hud";
		versionID = "F1irP06N";
		packwizDeps = [ m.ukulib.latestStable ];
		sha256 = "1e7e403e06e363d764bdaea02086f817e17fdc8a61084575413cf31e812f4bca";
		meta = {
			description = "Shows the armour slots in the HUD like the quickslots and offhand";
			license = licenses.mit; # https://github.com/uku3lig/armor-hud/blob/d6857af2ef17c236d1be38f058d8bca57f4183d0/LICENSE
		};
	};
	vectorientation = {
		modSlug = "vectorientation";
		versionID = "o1Vlfx1V";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "572010387dd87535b8af43b073879db82b8d5bea4141ff52536f38d96edea97d";
		meta = {
			description = "Gives falling blocks a fancy animation";
			license = licenses.cc0; # https://github.com/Tectato/Vectorientation/blob/fe71f9bac8c840307424cad61d9b6a25652d05f2/LICENSE
		};
	};
	visiblebarriers = {
		modSlug = "visiblebarriers";
		versionID = "HzB20fs3";
		packwizDeps = [ m.fabric-api.latestStable ];
		sha256 = "0f79519958bd69aef5dfdcec156ff888758ed04125feb10375f4e034f01698df";
		meta = {
			description = "Adds a toggle for making invisible blocks render similar to in creative mode, among other things";
			license = licenses.unfree /* all rights reserved */; # https://github.com/AmyMialeeMods/visiblebarriers/blob/5432ec578d36d494580f0b830b8d3a2b6e1fb2ca/LICENSE
		};
	};
	"x+-autofish" = {
		modSlug = "x+-autofish";
		versionID = "WhsTUm4d";
		packwizDeps = [ m.cloth-config.latestStable m.fabric-api.latestStable ];
		sha256 = "faef8cc66356b0badde013e682062bcd000b20e72cd7d6b428f9bbf361b21c27";
		meta = {
			description = "Automates fishing (arguably cheating)";
			license = licenses.gpl3Only; # https://github.com/Wudji/XPlus-AutoFish/blob/e7e9e8b6d2a573a096439e63177280794c4af435/LICENSE
			modID = "autofish";
		};
	};
	xaero-zoomout = {
		modSlug = "xaero-zoomout";
		versionID = "hcThI3QG";
		sha256 = "0fa5bab5449065f1ffe3f594063f28b10d57f4c95db5d2f3416e857abbd447a3";
		meta = {
			description = "Allows you to zoom out farther on Xaero's World Map";
			license = licenses.lgpl3Only; # https://github.com/NotRyken/XaeroZoomout/blob/d757c53fb86a69d85b3503466e353356255cb11c/COPYING.LESSER
			modID = "xaerozoomout";
		};
	};
}
