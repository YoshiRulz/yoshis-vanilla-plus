{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/23.11.tar.gz") {
	overlays = [ (import overlays/build-modrinth-pack.nix) (import overlays/minecraft-mods) ];
}
}: let
	f = _: drv: pkgs.mkShell {
		packages = [ pkgs.packwiz ];
		inputsFrom = [ drv ];
		shellHook = ''
			add() {
				packwiz mr add "$1"
				fgrep server mods/*.toml
			}
			exp() {
				packwiz mr export
			}
			upd() {
				if [ -e pack.toml ]; then
					for v in 1.20 1.20.1 1.20.2 1.20.3 1.20.4; do packwiz settings acceptable-versions -a $v; done
					packwiz update --all
				fi
			}
			cd '${builtins.toString drv.srcOnDisk}'
		'';
	};
	packs = pkgs.lib.mapAttrs f (pkgs.callPackage ./packs.nix {});
in packs // packs.pure
